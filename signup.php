<?php
  
  session_start();
  require 'config/database.php';
  require 'Model.php';

  $tipos_docente = $conn->prepare('SELECT id, nombre from tipo_profesores');
  $tipos_docente->execute();
  $tipos = $tipos_docente->fetchALL(PDO::FETCH_ASSOC);
  $modelo = new Model();

  $message = '';

  if (!empty($_POST['email']) && !empty($_POST['password'])) {

    $rfc = strtoupper($_POST['rfc']);
    $nombre = strtoupper($_POST['nombre']);
    $paterno = strtoupper($_POST['paterno']);
    $materno = strtoupper($_POST['materno']);
    $oficina = $_POST['oficina'];
    $celular = $_POST['celular'];
    $email = $_POST['email'];
    $tipo = $_POST['tipo'];
    $password = $_POST['password'];
    //$passHash = password_hash($password, PASSWORD_BCRYPT);
    $passHash = md5($password);
    $archivo = $_FILES['archivo'];

    $regex = '/^[A-Z]{4}([0-9]{2})(1[0-2]|0[1-9])([0-3][0-9])([ -]?)([A-Z0-9]{4})$/';
    $validacionRFC = preg_match($regex, $rfc);
    if(true) {
      
      
    

    $filtro = array('image/jpeg', 'image/png', 'application/pdf');
    $ext = pathinfo(basename($archivo["name"]), PATHINFO_EXTENSION);
    $ruta = '';
    if (in_array($archivo["type"], $filtro)){
      $ruta = "archivos/".(new \DateTime())->format('Y-m-d-H-i-s').".".$ext;
      move_uploaded_file($archivo['tmp_name'],$ruta);
    }
    if($ruta){
    
      $sql = "INSERT INTO usuarios (rfc,nombre,paterno,materno,oficina,celular,email, `password`,id_tipo_profesor,doc_grado) 
        VALUES ('$rfc','$nombre','$paterno','$materno','$oficina','$celular','$email','$passHash','$tipo','$ruta')";
    }
    else{
      $sql = "INSERT INTO usuarios (rfc,nombre,paterno,materno,oficina,celular,email,`password`,id_tipo_profesor) 
        VALUES ('$rfc','$nombre','$paterno','$materno','$oficina','$celular','$email','$passHash','$tipo')";
    }
    $stmt = $conn->prepare($sql);
    
    
    /*
    $stmt->bindParam(':rfc', $_POST['rfc']);
    $stmt->bindParam(':nombre', $_POST['nombre']);
    $stmt->bindParam(':paterno', $_POST['paterno']);
    $stmt->bindParam(':materno', $_POST['materno']);
    $stmt->bindParam(':oficina', $_POST['oficina']);
    $stmt->bindParam(':celular', $_POST['celular']);
    $stmt->bindParam(':email', $_POST['email']);
    $stmt->bindParam(':id_tipo_profesor', $_POST['tipo']);

    $password = $_POST['password'];
    $stmt->bindParam(':pass', $password);*/

    if ($stmt->execute()) { 
    /* CODIGO PARA EMAIL */
      $mail = new PHPMailer\PHPMailer\PHPMailer();
      $mail->isSMTP();
      $mail->SMTPAuth = true;
      $mail->SMTPSecure = 'tls';
      $mail->Host = 'smtp.gmail.com';
      $mail->Port = '587';
                       //465
      $mail->isHTML(true);
      $mail->CharSet = 'UTF-8';
      $mail->Username = 'uaemex.ico.validar@gmail.com';
      $mail->Password = 'Universidad19';
      $mail->SetFrom('uaemex.ico.validar@gmail.com','no-reply@ico.uaemex.mx');
      $mail->Subject = "CODIGO DE VERIFICACION";
      $mail->AddAddress($email);
      $sql = "SELECT id from usuarios where email = '$email'";
      $htmlStr = "";
     // $verificationCode = md5(uniqid("yourrandomstringyouwanttoaddhere", true));
     //$verificationCode = crc32(uniqid(rand(), true));
      //$verificationCodeSMS = crc32(uniqid(rand(), true));
      $verificationCode = $modelo->generateRandomString(10);
      $verificationCodeSMS = $modelo->generateRandomString(10);

    //$htmlStr .= "Hi " . $email . ",<br /><br />";
      $htmlStr .= "Hola " . $email . ",<br /><br /> Tu codigo de verificacion es el siguiente: ".$verificationCode . "<br /> Tu usuario es: " . $email . " <br /> Y tu contraseña es: " .$password;
      $mail->Body = $htmlStr;
      /* TERMINA CODIGO PARA EMAIL */        
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      $id_usuario = $stmt->fetch(PDO::FETCH_ASSOC)['id'];
     
              //*     CODIGO PARA SMS        *//
      $username = "jorgeramirezmanzanares@gmail.com"; 
      $hash = "3945f93e9409a476a699e06170284206fd42b643579e01df26a2b7b40a67b638"; 
      $test = "0"; 
      $sender = "API Test"; // This is who the message appears to be from. 
      $numbers = "52".$celular; // A single number or a comma-seperated list of numbers 
      $message = "Tu código de verificación es: ".$verificationCodeSMS; 

      $message = urlencode($message); 
      $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test; 
      $ch = curl_init('http://api.txtlocal.com/send/?'); 
      curl_setopt($ch, CURLOPT_POST, true); 
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
      $result = curl_exec($ch); // This is the result from the API 
      curl_close($ch); 
      echo($result); 
     //*   TEMRINA  CODIGO PARA SMS        *//
     if( !$mail->send()){
 
                echo "Mailer Error: " . $mail->ErrorInfo;

                die("Sending failed.");

            }else{
                    // tell the user a verification email were sent
                    //echo "<div id='successMessage'>A verification email were sent to <b>" . $email . "</b>, please open your email inbox and click the given link so you can login.</div>";
                    $messageSuccess = 'Se ha enviado un codigo de verificacion al correo' . $email . ' por favor, ingreselo en el siguiente apartado' ;

                    // save the email in the database
                    $created = date('Y-m-d H:i:s');

                    $sqlv = "INSERT INTO validacion (id_profesor,verificado,codigo_verificacion,tipo_verificacion,fecha_creacion) 
                    VALUES ('$id_usuario',0,'$verificationCode',0,'$created')";
                    $stmt1 = $conn->prepare($sqlv);
                    
                    $sqlv1 = "INSERT INTO validacion (id_profesor,verificado,codigo_verificacion,tipo_verificacion,fecha_creacion) 
                    VALUES ('$id_usuario',0,'$verificationCodeSMS',1,'$created')";
                    $stmt2 = $conn->prepare($sqlv1);
                    
 
                    // Execute the query
                    if($stmt1->execute() && $stmt2->execute()){

                        // echo "<div>Unverified email was saved to the database.</div>";
                    }else{
                        echo "<div>Unable to save your email to the database.";
                        //print_r($stmt->errorInfo());
                    }
            }
            $_SESSION['validar'] = $id_usuario;
            header('Location: /ihm/validate.php');
    } else {
      $messageAlert = "Ocurrió un error al hacer el registro";
    }
  }else{
    $messageAlert = "RFC inválido";
  }
  }
?>


<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Registro de usuario</title>
  <?php require 'partials/archivos.php' ?>
</head>

<body>
  <div class="container" id="app">
    <?php require 'partials/header.php' ?>
    <div class="card col-12">
      <div class="card-body">
        <img class="card-img-top" src="holder.js/100x180/" alt="">
        <div class="row">

          <div class="col-12">
            <h2 class="verde-oro text-light p-3 "><b>Registro de usuario</b></h2>
          </div>
          <br>

          <div class="col-12 text-center">
            <?php if(!empty($messageSuccess)): ?>
            <p class="alert alert-success"> <?= $messageSuccess ?></p>
            <br>
            <?php endif; ?>

            <?php if(!empty($messageAlert)): ?>
            <p class="alert alert-danger"> <?= $messageAlert ?></p>
            <br>
            <?php endif; ?>
          </div>

          <form class="col-12" action="signup.php" method="POST" enctype="multipart/form-data">
            <div>
              <div class="form-group row pt-3 ">
                <label for="nombre" class="col-sm-2 col-form-label"><b>Nombre(s)</b></label>
                <div class="col-sm-10">
                  <input required name="nombre" type="text" placeholder="Ingrese nombre" class="text-uppercase form-control">
                </div>
              </div>

              <div class="form-group row pt-1">
                <label for="paterno" class="col-sm-2 col-form-label"><b>Apellido paterno</b></label>
                <div class="col-sm-10">
                  <input required name="paterno" type="text" placeholder="Ingrese apellido paterno"
                    class="text-uppercase form-control">
                </div>
              </div>

              <div class="form-group row pt-1">
                <label for="materno" class="col-sm-2 col-form-label"><b>Apellido materno</b></label>
                <div class="col-sm-10">
                  <input required name="materno" type="text" placeholder="Ingrese apellido materno"
                    class="text-uppercase form-control">
                </div>
              </div>

              <div class="form-group row pt-1 ">
                <label for="rfc" class="col-sm-2 col-form-label"><b>RFC</b></label>
                <div class="col-sm-10">
                  <input required name="rfc" type="text" placeholder="Ingrese RFC" class="text-uppercase form-control" >
                  <small id="helpId" class="form-text text-muted">*EJEMPLO: AAAA000000XXXX</small>
                </div>
              </div>

              <div class="form-group row pt-1 ">
                <label for="oficina" class="col-sm-2 col-form-label"><b>Tipo de docente</b></label>
                <div class="col-sm-10">
                  <select name="tipo" class="form-control" required>
                    <option value="" selected disabled>SELECCIONE TIPO DE DOCENTE</option>
                    <option :value="tipo.id" v-for="tipo in tipos">
                      {{tipo.nombre}}
                    </option>
                  </select>
                </div>
              </div>



                <div class="form-group row pt-1 ">
                  <label for="oficina" class="col-sm-5 col-form-label"><b>Archivo de último grado de
                      estudios</b></label>
                  <div class="col-sm-7">
                    <input type="file" class="form-control-file" name="archivo" id="archivo"
                      placeholder="Cargue el archivo comprobante" aria-describedby="fileHelpId" required>
                    <small id="fileHelpId" class="form-text text-muted">*PDF,JPG o PNG</small></div>
                </div>

                

              <div class="form-group row pt-1 ">
                  <label for="oficina" class="col-sm-2 col-form-label"><b>Número de oficina</b></label>
                  <div class="col-sm-10">
                    <input required name="oficina" type="text" placeholder="INGRESE NÚMERO TELEFÓNICO DE OFICINA"
                      class="form-control" minlength="10" maxlength="10">
                  </div>
                </div>
  


                <div class="form-group row pt-1 ">
                  <label for="celular" class="col-sm-2 col-form-label"><b>Número de celular</b></label>
                  <div class="col-sm-10">
                    <input required name="celular" type="text" placeholder="INGRESE NÚMERO TELEFÓNICO DE CELULAR"
                      class="form-control" minlength="10" maxlength="10">
                    <small id="helpId" class="form-text text-muted">*Requiere verificación</small>

                  </div>
                </div>

                <div class="form-group row pt-1 ">
                  <label for="email" class="col-sm-2 col-form-label"><b>Correo electrónico</b></label>
                  <div class="col-sm-10">
                    <input required name="email" type="email" placeholder="INGRESE CORREO ELECTRÓNICO"
                      class="form-control">
                    <small id="helpId" class="form-text text-muted">*Requiere verificación</small>
                  </div>
                </div>

                <div class="form-group row pt-1 ">
                  <label for="password" class="col-sm-2 col-form-label"><b>Contraseña</b></label>
                  <div class="col-sm-10" >
                    <input required name="password" type="password" placeholder="INGRESE CONTRASEÑA"
                      class="form-control" minlength="5"  maxlength="16">

                  </div>
                </div>

                <div class="form-group row pt-1 ">
                  <label for="confirm_password" class="col-sm-2 col-form-label"><b>Confirmación de
                      contraseña</b></label>
                  <div class="col-sm-10">
                    <input required name="confirm_password" minlength="5" maxlength="16" type="password" placeholder="CONFIRME CONTRASEÑA"
                      class="form-control" >

                  </div>
                </div>

                <div class="centrado">
                  <input href="validate.php" class="btn verde" type="submit" value="Registrarse"> <br> <br>
                  <span><a href="login.php">Iniciar sesión</a></span>
                </div>
                <br>
              </div>
          </form>
          <br>

        </div>
      </div>
    </div>

    <?php require 'partials/footer.php' ?>

  </div>
  <script>
    let app = new Vue({
      el: "#app",
      data: {
        saludo: "s",
        tipos: JSON.parse('<?= json_encode($tipos)?>')
      },
      methods: {

      }
    });
  </script>
</body>

</html>