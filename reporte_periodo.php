<?php

require 'config/database.php';
require 'Model.php';

$modelo = new Model();

$id_periodo = $_GET['periodo'];
$periodo = $modelo->registro("SELECT nombre from periodos where id = $id_periodo")['nombre'];
$reporte = $modelo->reportePeriodo($id_periodo);

// Require composer autoload
require_once __DIR__ . '/vendor/autoload.php';
// Create an instance of the class:
$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);

$html = "
<!DOCTYPE html>
<html lang='es'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Reporte periodo $periodo </title>
    <style>
    table, th, td {
  border: 1px solid black;
}

th, td {
    padding: 15px;
  }
  td{
      width: 15%
  }
  .titulo-verde{
    background-color:#006400;
    color: white;
  }
  .titulo-oro{
    background-color:#9C8412;
    color: white;
    }
    </style>
</head>
<body>

<img src='assets/img/cabecera.jpeg' width='5%; ' />

<h4 style='text-align:center'>Plantilla de horarios $periodo</h4>";
$html .= "
    <table style='width:100%; text-align:center'>
    <tr  >
    <td class='titulo-verde'><b>Docente</b></td>
    <td class='titulo-verde'><b>Materia</b></td>
    <td class='titulo-verde'><b>Lunes</b></td>
    <td class='titulo-verde'><b>Martes</b></td>
    <td class='titulo-verde'><b>Miércoles</b></td>
    <td class='titulo-verde'><b>Jueves</b></td>
    <td class='titulo-verde'><b>Viernes</b></td>
    <td class='titulo-verde'><b>Sábado</b></td> 
 </tr>
 
";

foreach ($reporte as $registro) {
    $profe = $registro['paterno'].' '.$registro['materno'].' '.$registro['nombre'];
    foreach ($registro['horarios'] as $horario) {
        $html.= "<tr>";
            $html.= "<td>";
                $html.= $profe;
            $html.= "</td>"; 
            $html.= "<td>";
                $html.= $horario['materia'];
            $html.= "</td>";
        foreach ($horario['semana'] as $dia) {
            $html.= "<td>";
               if($dia['e'] && $dia['s']){
                    $html.= $dia['e'].' hrs - '.$dia['s'].' hrs';
               }else {
                   $html.= "";
               } 
            $html.= "</td>";        
        }
        $html.= "</tr>";
    }
}


$html .= "</table>
</body>
</html>
";

//echo $html;
// Write some HTML code:
$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output();
//echo json_encode($reporte);


?>