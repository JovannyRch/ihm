<?php
    require 'Model.php';
  session_start();
  require 'config/database.php';


  $modelo = new Model();
  $periodo_actual = $modelo->getPeriodoActual()['nombre'];

  

  if (isset($_SESSION['user_id'])) {
    header('Location: /ihm/index.php');
  }



  if( isset($_SESSION['msg'])){
    $msg = $_SESSION['msg'];
  }

  


  if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $mail = $_POST['email'];
    $pass = $_POST['password'];
    $records = $conn->prepare("SELECT id, email, password, tipo_usuario FROM usuarios WHERE email = :email");
    $records1 = $conn->prepare("SELECT password FROM usuarios WHERE email = '$mail'");

    //$password_bd= $records1->fetch(PDO::FETCH_ASSOC)['password'];
    $records->bindParam(':email', $_POST['email']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    $password_bd = $results['password'];

    $encriptada = md5($pass);
    if ($password_bd == $encriptada) {
        $_SESSION['user_id'] = $results['id'];
        $tipo_usuario = $results['tipo_usuario'];
        if($tipo_usuario == "1"){

            $sql = "SELECT verificado from validacion INNER JOIN usuarios on validacion.id_profesor = usuarios.id WHERE email = '$mail' AND tipo_verificacion = 0";
            $sql1 = "SELECT verificado from validacion INNER JOIN usuarios on validacion.id_profesor = usuarios.id WHERE email = '$mail' AND tipo_verificacion = 1";
            
            $stmt = $conn->prepare($sql);
            $stmt1 = $conn->prepare($sql1);

            if ($stmt->execute() && $stmt1->execute()) { 
                $verificado= $stmt->fetch(PDO::FETCH_ASSOC)['verificado'];
                $verificado1= $stmt1->fetch(PDO::FETCH_ASSOC)['verificado'];

                if ($verificado == 1 && $verificado1 == 1) {
                    header("Location: /ihm/index.php"); 
                } else{
            //echo "TIENES QUE VALIDAR TU CORREO Y/O TELEFONO";
                $_SESSION['validar'] = $results['id'];
                header("Location: /ihm/validate.php");
            }
                
            } else{
            //echo "TIENES QUE VALIDAR TU CORREO Y/O TELEFONO";
                $_SESSION['validar'] = $results['id'];
                header("Location: /ihm/validate.php");
            }
        }
  
        if($tipo_usuario == "2"){
          
          header("Location: /ihm/index_admin.php");
        }

    
  

  }else{

   // echo "Datos no econtrados";
    $msg = array("tipo" => "warning", "texto" => "Datos no encontrados");
  }
  }
?>

    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Inicio de sesión</title>

        <?php require 'partials/archivos.php' ?>

    </head>

    <body>
        <div class="container" id="app">

            <?php require 'partials/header.php' ?>
            <?php if(!empty($message)): ?>
            <p>
                <?= $message ?>
            </p>
            <?php endif; ?>


            <?php if(isset($msg)): ?>
            <p class="text-center alert alert-<?= $msg['tipo'] ?>">
                <?= $msg['texto'] ?>
            </p>

            <?php 
      $_SESSION['msg'] = null;
  endif; ?>


            <div class="row">
                <div class="col-12 col-md-7 d-none d-lg-block">
                    <img src="assets/img/vitral.jpg" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="">
                </div>
                <div class="col-12 col-md-5 p-sm-4">

                    <h2 class="text-center text-verde"><b>Solicitud de Ingreso a Plantilla <?=$periodo_actual ?> </b></h2>
                    <div class="linea-vo">

                    </div>
                    <form action="login.php" method="POST" class="d-flex justify-content-center mt-5">

                        <div class="centrado" style="width: 100%">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"> <i class="fa fa-envelope" aria-hidden="true"></i>
                </span>
                                </div>
                                <input type="email" name="email" class="form-control" placeholder="Correo electrónico" aria-label="Username" aria-describedby="basic-addon1">
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"> <i class="fa fa-lock " aria-hidden="true"></i>
                </span>
                                </div>
                                <input type="password" name="password" class="form-control" placeholder="Contraseña" aria-label="pass" aria-describedby="basic-addon1">
                            </div>

                            <button type="submit" class="btn btn-block verde" > INICIAR SESIÓN</button>
                        </div>
                    </form>
                    <br> <br>

                    <div class="col-12 text-center ">
                        <span><a class="text-success" data-toggle="modal" data-target="#recuperarPass" @click="correo = ''">¿Olvidaste tu contraseña?</a></span> <br>
                        <span> ¿No te has registrado? <a href="signup.php" class="text-success"> Registrate aquí</a></span>
                    </div>
                </div>
            </div>



            <!-- Modal -->
            <div class="modal fade" id="recuperarPass" tabindex="-1" role="dialog" aria-labelledby="rid" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Recuperar contraseña</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                        </div>
                        <form @submit.prevent="enviarPass">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="email">Por favor ingresa tu correo</label>
                                    <input required type="email" v-model="correo" class="form-control" name="email" id="email" aria-describedby="helpID" placeholder="Correo">
                                    <small id="helpID" class="form-text text-muted">* Enviaremos una nueva contraseña a tu correo</small>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn verde text-light">Enviar</button>

                                <button type="button" class="btn oro text-light" data-dismiss="modal">Cerrar</button>
                            </div>
                        </form>
                    </div>
                </div>
               
        </div>
     


        <?php require 'partials/footer.php' ?>

        </div>
        <?php require 'partials/footerarchivos.php' ?>

        <script>
            var app = new Vue({
                el: '#app',
                data: {
                    correo: ''
                },
                methods: {
                    enviarPass() {

                        $.ajax({
                            type: "POST",
                            url: 'controladores/api.php',
                            data: {
                                servicio: "recuperarPass",
                                correo: this.correo,
                            },
                            success: function(response) {
                                respuesta = JSON.parse(response);
                                if (respuesta) {
                                    $.notify(
                                        "Hemos enviado tu nueva contraseña a tu correo", {
                                            position: "right top",
                                            className: "success"
                                        }
                                    );
                                } else {
                                    $.notify(
                                        "Correo no encontrado", {
                                            position: "right top",
                                            className: "danger"
                                        })
                                }
                                $('#recuperarPass').modal('hide');
                            }
                        });
                    }
                }
            });
        </script>

    </body>

    </html>