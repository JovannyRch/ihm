<?php
  require 'modelos/inicio_administrador.php';
?>


<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Inicio</title>

  <?php require 'partials/archivos.php' ?>

</head>

<body>
  <div class="container bg-white" id="app">

    <?php require 'partials/header.php' ?>


    <h2 class="verde-oro text-light p-3"><b>Administración </b></h2>

    <div class="row">
      <div class=" col-12 col-lg-12">
        <div class="">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">



            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" @click="pagina = 1" href="#">Profesores<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link" @click="pagina = 3" href="#">Periodos<span class="sr-only">(current)</span></a>
                </li>
               
              </ul>
              <div class="form-inline my-2 my-lg-0">
                <span class="pull-right"><a href="logout.php"> <i class="fa fa-sign-out"></i> Cerrar sesión</a></span>
                <br>
              </div>
            </div>
          </nav>

        </div>
      </div>
      <div class="card col-12 p-4 ">

        <div v-if="pagina == 1">
          <h4 class="text-verde"> <b>Lista de profesores</b></h4>
          <div class="linea-vo">
          </div>

          <div class="col-auto mt-4 col-7  centrado">
            <label class="sr-only" for="inlineFormInputGroup">Username</label>
            <div class="input-group mb-2">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="fa fa-search" aria-hidden="true"></i>
                </div>
              </div>
              <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Buscar docente"
                v-model="buscador">
            </div>
          </div>

          <table class="table table-striped" v-if="profesores.length">
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Datos de contacto</th>
                <th>Tipo profesor</th>
                <th>Rango (horas)</th>
                <th>Tiempo registrado</th>
                <th>Estado</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(profesor,index) in filtro">
                <td scope="row">{{index+1}}</td>
                <td>{{profesor.nombre}} {{profesor.paterno}} {{profesor.materno}}</td>
                <td>
                  <i class="fa fa-envelope" aria-hidden="true"></i> {{profesor.email}} <br>
                  <i class="fa fa-phone" aria-hidden="true"></i> {{profesor.celular}}
                </td>
                <td>{{profesor.tipo_profesor}}</td>
                <td>
                  <div v-if="profesor.minH && profesor.maxH">
                    {{profesor.minH}} - {{profesor.maxH}}
                  </div>
                  <div v-if=" !(profesor.minH && profesor.maxH) && profesor.maxH">
                    Max: {{profesor.maxH}}
                  </div>
                </td>
                <td>
                  {{formatMin(profesor.tiempo.minutos)}}
                </td>
                <td>
                  <div v-if="profesor.minH && profesor.maxH">
                    <div
                      v-if="profesor.tiempo.minutos >= profesor.minH*60 &&  profesor.tiempo.minutos<= profesor.maxH*60"
                      class="alert alert-success">

                    </div>
                    <div v-else class="alert alert-danger">

                    </div>
                  </div>

                  <div v-if=" !(profesor.minH && profesor.maxH) && profesor.maxH">
                    <div v-if="profesor.tiempo.minutos<= profesor.maxH*60" class="alert alert-success">

                    </div>
                    <div v-else class="alert alert-danger">

                    </div>
                  </div>

                </td>
                <td>
                  <a href="#" @click="getProfesor(profesor)"> Ver plantilla </a>
                </td>
              </tr>

            </tbody>
          </table>
        </div>

        <div v-if="pagina == 2" class="row">
          <div class="col-12">
            <button class="btn btn-default mb-3" @click="pagina = 1"> <i class="fa fa-arrow-left"></i> Regresar
            </button></b>
          </div>
          <div class="col-12 col-lg-3 border pt-2">
            Datos del profesor <br>
            <div class="linea-ov mb-2"></div>
            <b>Nombre:</b> <br> {{profesor.nombre}} {{profesor.paterno}} {{profesor.materno}} <br>
            <b>Email:</b> <br> {{profesor.email}} <br>
            <b>Celular:</b> <br> {{profesor.celular}} <br>
            <b>Número de oficina:</b> <br> {{profesor.oficina}} <br>
            <b>RFC:</b> <br> {{profesor.rfc}} <br>
            <br>
            <div class=" col-12">
              <a href="#" class="text-oro" data-toggle="modal" @click="getTiposUsuario()" data-target="#editarDatos"> <i
                  class="fa fa-pencil"></i> Editar Datos
              </a>
              <br>
              <a href="#" class="text-oro" data-toggle="modal" data-target="#visualizador">
                Ver comprobante grado de estudios
              </a>
            </div>
            <br>
            Estado <br>
            <div class="linea-ov mb-2"></div>
            <div v-if="profesor.minH && profesor.maxH">
              <div v-if="profesor.tiempo.minutos >= profesor.minH*60 &&  profesor.tiempo.minutos<= profesor.maxH*60"
                class="alert alert-success">
                Tiempo laboral válido
              </div>
              <div v-else class="alert alert-danger">
                Tiempo laboral inválido
              </div>
            </div>

            <div v-if=" !(profesor.minH && profesor.maxH) && profesor.maxH">
              <div v-if="profesor.tiempo.minutos<= profesor.maxH*60" class="alert alert-success">
                Tiempo laboral válido
              </div>
              <div v-else class="alert alert-danger">
                Tiempo laboral inválido
              </div>
            </div> <br>
            Tipo de docente <br>
            <div class="linea-ov mb-2"></div>
            <div>{{profesor.tipo_profesor}}</div> <br>
            Horario laboral <br>
            <div class="linea-ov mb-2"></div>
            <div v-if="profesor.minH && profesor.maxH">
              <b>Rango:</b> <br> {{profesor.minH}} horas- {{profesor.maxH}} horas
            </div>
            <div v-if=" !(profesor.minH && profesor.maxH) && profesor.maxH">
              <b>Rango:</b> <br> Max: {{profesor.maxH}}
            </div>
            <b>Tiempo registrado: </b> <br>
            {{formatMin(profesor.tiempo.minutos)}}
            <br>

          </div>

          <div class=" col-12 col-lg-9 mt-sm-4">
            <h4 class="card-title text-verde text-center"> <b>Plantilla de horarios
                <span v-if="periodos.length > 0 && periodos[0].actual == 1">
                  {{periodos[0].nombre}}</b>
              </span>
            </h4>
            <div v-if="!horarios.length" class="centrado text-verde">
              <h3>Sin registros</h3>
            </div>

            <div v-else
              :class="h.status==1?'card col-12 mt-2 d-none d-lg-block alert-verde-1':'card col-12 mt-2 d-none d-lg-block'"
              v-for="h in horarios">
              <div class="card-body row">

                <div class="col-12">
                  
                    <div  class="col-12 pull-right mb-1 text-center"  v-if="h.status == 1 || h.status == 2">
                        <div class="alert alert-verde-pastel alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <strong v-if="h.status == 1" >Horario aprobado por la administración</strong>
                          <strong v-if="h.status == 2" >Horario fijado por ambas partes</strong>
                        </div>
    
    
                       
                      </div>

                      <div  class="col-12 pull-right mb-1 text-center"  v-if="h.status == 0">
                        <div class="alert alert-dismissible fade show" style="background-color: rgb(223, 217, 146)" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <strong >En espera </strong>
                        </div>
    
    
                       
                      </div>

                      
                  <div class="col-12 pull-right mb-2" v-if="h.status != 0">
                    <button @click="selecionarHorario(h)" style="background-color: rgb(189, 65, 71); color: white"
                      class="btn pull-right btn-sm" data-toggle="modal" data-target="#confirmarEliminacion"> <i
                        class="fa fa-trash"></i> </button>

                    <button @click="selecionarHorario(h)"
                      style="background-color: rgba(214, 196, 33, 0.753); color: white"
                      class="btn pull-right btn-sm mr-1" data-toggle="modal" data-target="#editarHorario"> <i
                        class="fa fa-edit"></i> </button>

                    <button v-if="h.status == 3" @click="setStatusH(1,h.id)" style=" background-color: rgb(34, 151, 87);
                      color: white" class="btn pull-right btn-sm mr-1">
                      <i class="fa fa-check" aria-hidden="true"></i> Aprobar
                    </button>

                    <button v-else @click="setStatusH(0,h.id)" class="btn pull-right btn-sm mr-1 btn-default">
                      <i class="fa fa-times" aria-hidden="true"></i> Quitar aprobación
                    </button>

                  </div>

                  

                  <h5 class="text-verde"><b>{{h.materia}} ({{h.plan}})</b></h5>
                  <div class="linea-vo">
                  </div>


                  <div class="d-flex flex-row pt-3 col-12 hidden-sm-down" v-if="h.semana.length">
                    <div v-for="dia in h.semana" v-if="dia.e && dia.s" class="border p-1 mr-1">
                      <span class="form-control text-center mb-auto"
                        style="background-color: #9C8412; color: white">{{getDia(dia.d)}}</span>
                      <p class=" text-center"><i v-if="dia.e && dia.s"> <i class="fa fa-clock-o" aria-hidden="true"></i>
                          <br> {{dia.e}} - {{dia.s}}</i> </p>
                      <p class=" text-center " data-toggle="tooltip" data-placement="top" :title="dia.lugar">
                        <i class="fa fa-map-marker" aria-hidden="true"></i> <br> {{dia.sigla}}
                      </p>
                    </div>
                  </div>

                  <div v-if="!h.semana.length">
                    <div class="alert " role="alert">
                      <h6 class="alert-heading text-center">Agrege clases a la materia</h6>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 pt-3">
                      <h6 class="text-verde">Total horas: {{formatMin(h.total)}}</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="d-sm-block  d-lg-none d-xl-none d-sm-none">
              <div class="card mb-4" v-for="h in horarios">
                <div class="card-body">

                  <div class="col-12 pull-right" v-if=>
                    <button @click="selecionarHorario(h)" style="background-color: rgb(189, 65, 71); color: white"
                      class="btn pull-right btn-sm" data-toggle="modal" data-target="#confirmarEliminacion"> <i
                        class="fa fa-trash"></i> </button>
                    <button @click="selecionarHorario(h)"
                      style="background-color: rgba(214, 196, 33, 0.753); color: white"
                      class="btn pull-right btn-sm mr-1" data-toggle="modal" data-target="#editarHorario"> <i
                        class="fa fa-edit"></i> </button>

                    <button v-if="h.status == 0" @click="setStatusH(1,h.id)" style=" background-color: rgb(34, 151, 87);
                      color: white" class="btn pull-right btn-sm mr-1">
                      <i class="fa fa-check" aria-hidden="true"></i> Aprobar
                    </button>

                    <button v-else @click="setStatusH(0,h.id)" class="btn pull-right btn-sm mr-1 btn-default">
                      <i class="fa fa-times" aria-hidden="true"></i> Quitar aprobación
                    </button>
                  </div>

                  <h4 class="text-verde"> <b>{{h.materia}} ({{h.plan}})</b></h4>
                  <div class="linea-vo">
                  </div>
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item" v-for="dia in h.semana" v-if="dia.e && dia.s">
                    {{getDia(dia.d)}} / <span><i>{{dia.e}} - {{dia.s}} / {{dia.l}}</i></span>


                  </li>
                  <li class="list-group-item">
                    <div class="row">
                      <div class="col-12">
                        <div class="alert alert-dark pull-right m-0 p-1" role="alert"
                          style="width: 40%;margin-top: 0%; padding-top: 0%">
                          <small>Tiempo acomulado: {{formatMin(h.total)}}</small>
                        </div>
                      </div>


                    </div>
                  </li>
                </ul>
              </div>
            </div>

          </div>


        </div>
        <div v-if="pagina == 3">


          <h4 class="text-verde"> <b>Periodos</b></h4>
          <div class="linea-vo">
          </div> <br>

          <!-- Button trigger modal -->
          <div class="col-12">

            <span class="badge badge-pill badge-primary  pull-right mb-3">
              <button type="button" class="btn text-light btn-sm" data-toggle="modal" data-target="#agregarPeriodo"
                click="nuevoPeriodo()">
                <i class="fa fa-plus"></i>
              </button>
            </span>
          </div>

          <div class="col-s12 mb-2" v-if="periodos.length > 0 && periodos[0].actual == 1">
            <b> Periodo actual: </b> {{periodos[0].nombre}}
          </div>
          <div class="col-s12 mb-2" v-else>
            <b> No hay periodo actual activado</b>
          </div>

          <ul class="list-group col-12">
            <li v-for="p in periodos" class="list-group-item d-flex justify-content-between align-items-center"
              :style="p.actual === '1'? 'background-color: rgb(136, 236, 170,0.4)': ''">
              {{p.nombre}}
              <div class="pull-right">
                <span class="badge badge-success badge-pill" v-if="p.actual === '0'">
                  <button class="btn btn-sm text-light" @click="setPeriodo(p.id,1)">
                    <i class="fa fa-check"></i>
                  </button>
                </span>
                <span class="badge badge-warning badge-pill" v-else>
                  <button class="btn  btn-sm" @click="setPeriodo(p.id,0)"> <i class="fa fa-times"></i> </button>
                </span>

                <span class="badge badge-info badge-pill">
                  <button class="btn  btn-sm text-light" @click="editarPeriodo(p)" data-toggle="modal"
                    data-target="#agregarPeriodo"> <i class="fa fa-pencil"></i> </button>
                </span>

                <span class="badge badge-danger badge-pill">
                  <button class="btn  btn-sm text-light" @click="deletePeriodo(p.id)"> <i class="fa fa-trash"></i>
                  </button>
                </span>

                <span class="badge badge-success badge-pill">
                    <a class="btn  btn-sm text-light" target="_blank " :href="`reporte_periodo.php?periodo=${p.id}`" > Generar reporte
                    </a>
                  </span>

              </div>

            </li>

          </ul>
        </div>
      </div>


      <!-- Modal -->
      <div class="modal fade" id="agregarPeriodo" tabindex="-1" role="dialog" aria-labelledby="periodoID"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">
                <span v-if="modoPeriodo == 1"> Agregar periodo </span>
                <span v-if="modoPeriodo == 2"> Editar periodo </span>
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form @submit.prevent="savePeriodo()">

              <div class="modal-body">

                <div class="form-group">
                  <label for="periodoNombre">Nombre</label>
                  <input v-model="periodo" type="text" class="form-control" name="periodoNombre" id="periodoNombre"
                    aria-describedby="pNombre" placeholder="Escriba el nombre del periodo">
                </div>
              </div>


              <div class="modal-footer">
                <button type="submit" class="btn verde">
                  <span v-if="modoPeriodo == 1">Agregar</span>
                  <span v-if="modoPeriodo == 2">Guardar cambios</span>
                </button>
                <button type="button" class="btn oro" data-dismiss="modal">Cerrar</button>

              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="visualizador" tabindex="-1" role="dialog" aria-labelledby="viewer" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Visualizador de archivos</h5>

              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid" v-if="profesor.doc_grado">
                <embed :src="profesor.doc_grado" height="400px" width="100%"
                  v-if="endsWith(profesor.doc_grado,'pdf')" />
                <img :src="profesor.doc_grado" height="400px" width="100%"
                  v-if="endsWith(profesor.doc_grado,'jpg') || endsWith(profesor.doc_grado,'png') || endsWith(profesor.doc_grado,'jpeg')" />
              </div>
              <div v-else>
                No se encontró ningún documento
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn oro btn-sm" data-dismiss="modal">Cerrar</button>

            </div>
          </div>
        </div>
      </div>


      <!-- Modal -->
      <div class="modal fade" id="editarDatos" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title ">Editar datos personales</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="" id="editarDatosForm" method="post" enctype="multipart/form-data">
              <div class="modal-body row">
                <div class="form-group col-12 col-md-6">
                  <label for="">Nombre completo</label>
                  <input type="text" class="form-control-plaintext" name="" id="" aria-describedby="helpId"
                    :value="userAux.nombre+' '+userAux.materno+''" readonly>
                </div>
                <div class="form-group col-12 col-md-6">
                  <label for="emailEdit">Email</label>
                  <input type="email" class="form-control-plaintext" name="emailEdit" id="emailEdit"
                    aria-describedby="emailHelp" placeholder="Escribe tu correo" v-model="userAux.email" readonly>
                </div>

                <div class="form-group col-12 col-md-6">
                  <label for="celularEdit">Celular</label>
                  <input type="number" class="form-control" name="celularEdit" id="celularEdit"
                    aria-describedby="emailHelp" placeholder="Escribe tu número celular" v-model="userAux.celular">
                  <small id="emailHelp" class="form-text text-muted">Requiere verificación de número</small>
                </div>

                

                <div class="form-group  col-12 col-md-6">
                  <label for="tipoDocente">Tipo de docente</label>
                  <select class="form-control" name="tipoDocente" id="tipoDocente" required v-model="userAux.id_tipo">
                    <option v-for="tipo in tiposUsuario" :value="tipo.id">{{tipo.nombre}}</option>
                  </select>
                </div>

                <div class="form-group col-12 col-md-6">
                  <label for="archivo">Archivo comprobante de último grado de estudios</label>
                  <input type="file" class="form-control-file" name="archivo" id="archivo"
                    placeholder="Cargue el archivo comprobante" aria-describedby="fileHelpId">
                  <small id="fileHelpId" class="form-text text-muted">*PDF,JPG o PNG</small>
                </div>
                <input type="text" class="collapse" value="subirArchivo" name="servicio" id="servicio">
                <input type="text" class="collapse" :value="profesor.id" name="id_usuario" id="id_usuario">





              </div>
              <div class="modal-footer">
                <button type="button" class="btn verde  btn-sm" onclick="subirArchivos()">Actualizar</button>
                <button type="button" class="btn oro  btn-sm" data-dismiss="modal">Cancelar</button>

              </div>
            </form>

          </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="confirmarEliminacion" tabindex="-1" role="dialog" aria-labelledby="sss"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Confirmar eliminación</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body centrado">
              ¿Estás seguro de eliminar el horario de la materia
              <b>{{horarioSeleccionado.materia}}</b>?
            </div>
            <div class="modal-footer ">
              <button type="button" class="btn verde  btn-sm" data-dismiss="modal"
                @click="eliminarHorario(horarioSeleccionado.id)">Eliminar</button>
              <button type="button" class="btn oro  btn-sm" data-dismiss="modal">Cancelar</button>

            </div>
          </div>
        </div>
      </div>



      <!-- Modal -->
      <div class="modal fade" id="editarHorario" tabindex="-1" role="dialog" aria-labelledby="editH" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Editar horario</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">

                <div class="row">
                  <div :class="horarioSeleccionado.porcentaje == 100? 'col-12':'col-8'">

                    <div class="d-flex flex-row pt-3 col-12 hidden-sm-down" v-if="horarioSeleccionado.semana.length">
                      <div v-for="(dia,index) in horarioSeleccionado.semana" class="border p-1 mr-1">
                        <span class="form-control text-center mb-auto"
                          style="background-color: #9C8412; color: white">{{getDia(dia.d)}}</span>
                        <p class=" text-center"><i v-if="dia.e && dia.s">{{dia.e}} - {{dia.s}}</i> </p>
                        <p class=" text-center border">{{dia.sigla}}</p>

                        <button @click="quitarClaseEdit(index)" class="btn  btn-sm border pull-right"
                          style="background-color: rgb(218, 216, 210)"> <i class="fa fa-times"></i> </button>

                      </div>

                    </div>

                    <div v-if="!horarioSeleccionado.semana.length" style="height: 50%;">
                      <div class="alert mt-4" role="alert">
                        <h6 class="alert-heading text-center">Agrege clases a la materia</h6>
                      </div>
                    </div>

                    <div class="row">
                      <div class="progress col-12 mt-3 mb-3 ">
                        <div class="progress-bar bg-success" role="progressbar"
                          :style="'width: '+horarioSeleccionado.porcentaje+'%'" aria-valuenow="25" aria-valuemin="0"
                          aria-valuemax="100">
                          {{horarioSeleccionado.porcentaje}}%</div>
                      </div>



                      <div class="col-12">
                        <div class="alert alert-dark pull-right m-0 p-0" role="alert"
                          style="width: 40%;margin-top: 0%; padding-top: 0%">
                          <small>Tiempo acomulado: {{formatMin(horarioSeleccionado.total)}}</small>
                        </div>
                      </div>

                      <div class="col-12">
                        <div :class="getClase(controlMateria.porcentaje)" role="alert"
                          style="width: 40%;margin-top: 0%; padding-top: 0%">
                          <small>Tiempo restante:
                            {{formatMin(parseInt(horarioSeleccionado.minutos) - horarioSeleccionado.total)}}</small>
                        </div>
                      </div>

                    </div>


                  </div>

                  <div class="col-md-4" v-if="horarioSeleccionado.porcentaje != 100">

                    <div class="form-group col-12">
                      <label for="diaS">Día</label>
                      <select class="form-control" name="diaS" id="diaS" v-model="clase.dia">
                        <option selected value="-1" disabled>Seleccione un día</option>
                        <option value="1">Lunes</option>
                        <option value="2">Martes</option>
                        <option value="3">Miércoles</option>
                        <option value="4">Jueves</option>
                        <option value="5">Viernes</option>
                        <option value="6">Sábado</option>
                      </select>
                    </div>

                    <div class="form-group col-12">
                      <label for="lugars">Lugar</label>
                      <select class="custom-select" name="lugars" id="lugars" v-model="clase.lugar">
                        <option selected disabled :value="lugarNull">Seleccione lugar</option>
                        <option :value="l" v-for='l in lugares'>{{l.nombre}}</option>
                      </select>
                    </div>


                    <div class="form-group  col-12">
                      <label for="he">Hora de entrada</label>
                      <select class="form-control" name="he" id="he" v-model="horaEntrada">
                        <option selected value="-1" disabled>Seleccione hora de entrada</option>
                        <option v-for="i in (horas.length-1)" :value="i-1">{{horas[i-1]}} </option>
                      </select>
                    </div>
                    <div class="form-group  col-12" v-if="horaEntrada != -1">
                      <label for="he">Hora de salida</label>
                      <select class="form-control" name="he" id="he" v-model="horaSalida">
                        <option selected value="-1" disabled>Seleccione hora de salida</option>
                        <option v-for="i in ((horarioSeleccionado.minutos-horarioSeleccionado.total)/30)"
                          :value="i+horaEntrada">
                          {{horas[i+horaEntrada]}}
                        </option>
                      </select>
                    </div>

                    <div class="alert alert-light text-center col-12" role="alert"
                      v-if="horaEntrada != -1 && horaSalida != -1">
                      <strong> Duración: {{toHoras(horaSalida,horaEntrada)}} </strong>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button v-if="horarioSeleccionado.porcentaje == 100" type="button" class="btn verde btn-sm"
                @click="editarHorario()">Guardar cambios</button>
              <button v-else type="button" class="btn verde btn-sm" @click="agregarClaseEdit">Agregar clase</button>
              <button type="button" class="btn oro btn-sm" data-dismiss="modal">Cancelar</button>

            </div>
          </div>
        </div>
      </div>


    </div>

    <?php require 'partials/footer.php' ?>

  </div>
  <?php require 'partials/footerarchivos.php' ?>

  <script>
    function subirArchivos() {
      var parametros = new FormData($("#editarDatosForm")[0]);

      $.ajax({
        type: "POST",
        url: 'controladores/api.php',
        data: parametros,
        contentType: false,
        processData: false,
        success: function (response) {
          respuesta = JSON.parse(response);
          app.usuario = respuesta.usuario;
          $.notify(
            "Datos actualizados", {
              position: "right top",
              className: "success"
            }
          );
        }
      });
    }

    let app = new Vue({
      el: "#app",
      data: {
        usuario: JSON.parse('<?= json_encode($datos )?>'),
        userAux: {url:''},
        pagina: 1,
        profesores: [],
        profesor: {},
        horarios: [],
        periodos: [],
        periodo: '',
        modoPeriodo: 1,
        auxidPeriodo: null,
        periodoActual: {},
        buscador: '',
        clase: {
          dia: -1,
          id_lugar: -1,
          entrada: '',
          salida: '',
          tiempo: 0,
          lugar: {
            id: '',
            nombre: ''
          }
        },
        horaEntrada: -1,
        horaSalida: -1,
        horarioSeleccionado: {
          materia: '',
          semana: []
        },
        modelMateria: {
          id: ''
        },
        controlMateria: {
          total: 0,
          porcentaje: 0,
          semana: []
        },
        lugarNull: {
          id: '',
          nombre: ''
        },
        lugares: [],
        horas: [
          '7:00',
          '7:30',
          '8:00',
          '8:30',
          '9:00',
          '9:30',
          '10:00',
          '10:30',
          '11:00',
          '11:30',
          '12:00',
          '12:30',
          '13:00',
          '13:30',
          '14:00',
          '14:30',
          '15:00',
          '15:30',
          '16:00',
          '16:30',
          '17:00',
          '17:30',
          '18:00',
          '18:30',
          '19:00',
          '19:30',
          '20:00',
          '20:30',
          '21:00',
          '21:30',
          '22:00',
        ],
        tiposUsuario: [],
        plantilla: {
          id: '',
          estado: ''
        }
      },
      created: function () {
        this.getProfesores();
        this.getPeriodos();
        this.getLugares();
      },
      methods: {
        nuevoPeriodo() {
          this.modoPeriodo = 1;
          this.periodo = '';
        },
        getTiposUsuario() {
          this.userAux = Object.assign({}, this.profesor);
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "getTipos"
            },
            success: function (response) {
              respuesta = JSON.parse(response);
              app.tiposUsuario = respuesta;
            }
          });
        },
        getClase(porcentaje) {
          clase = 'pull-right m-0 p-0 alert ';

          if (porcentaje <= 50) {
            clase += 'alert-danger';
          }

          if (porcentaje <= 90 && porcentaje > 50) {
            clase += 'alert-warning';
          }

          if (porcentaje < 100 && porcentaje > 90) {
            clase += 'alert-warning';
          }

          if (porcentaje == 100) {
            clase += 'collapse';
          }

          return clase;
        },
        getLugares() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "getLugares"
            },
            success: function (response) {
              var respuesta = JSON.parse(response);
              app.lugares = respuesta;
            }
          });
        },
        eliminarHorario(id_horario) {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "eliminarHorario",
              id_horario: id_horario
            },
            success: function (response) {
              $.notify(
                "Eliminación exitosa", {
                  position: "right top",
                  className: "info"
                }
              );
              app.getProfesor(app.profesor)
            }
          });
        },
        selecionarHorario(horario) {

          this.clase = {
            dia: -1,
            id_lugar: -1,
            entrada: '',
            salida: '',
            tiempo: 0,
            lugar: {
              id: '',
              nombre: ''
            }
          };
          this.horaEntrada = -1;
          this.horaSalida = -1;
          this.horarioSeleccionado = Object.assign({}, horario);

          this.horarioSeleccionado.porcentaje = (parseInt(this.horarioSeleccionado.minutos) * 100 / parseInt(this
            .horarioSeleccionado.total)).toFixed(2);
        },
        endsWith(cadena, ext) {
          return cadena.endsWith(ext);
        },
        setStatusH(estado, id_horario) {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "setStatusH",
              id_horario: id_horario,
              estado: estado
            },
            success: function (response) {
              if (estado == 0) {
                $.notify(
                  "Se ha quitado la aprobación del horario", {
                    position: "right top",
                    className: "warn"
                  }
                );
              } else {
                $.notify(
                  "Se ha aprobado el horario", {
                    position: "right top",
                    className: "success"
                  }
                );
              }
              app.getProfesor(app.profesor)
            }
          });
        },
        definirStatus(usuario) {
          statusProfe = {
            clase: '',
            mensaje: '',
            inRango: false
          };
          if (usuario.minH && usuario.maxH) {
            if (usuario.tiempo.minutos < usuario.minH * 60) {
              statusProfe.clase = "alert alert-danger mt-3";
              statusProfe.mensaje = "Fuera de rango <br> Tiempo faltante: " + this.formatMin(usuario
                .minH * 60 - usuario.tiempo.minutos);
              statusProfe.inRango = false;
              return statusProfe;
            }

            if (usuario.tiempo.minutos >= usuario.minH * 60 && usuario.tiempo.minutos < usuario.maxH * 60) {
              statusProfe.clase = "alert alert-success mt-3";
              statusProfe.mensaje = "Dentro del rango <br> Tiempo adicional permitido: " + this.formatMin(this
                .usuario.maxH * 60 - usuario.tiempo.minutos);
              statusProfe.inRango = true;
              return statusProfe;
            }

            if (usuario.tiempo.minutos == usuario.maxH * 60) {
              statusProfe.clase = "alert alert-success mt-3";
              statusProfe.mensaje = "Ha llegado al maximo de horas clase permitidas";
              statusProfe.inRango = true;
              return statusProfe;
            }
            return {
              clase: '',
              mensaje: '',
              inRango: false
            };
          }
        },
        getDatosProfe() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "getDatosProfe",
              id_usuario: this.profesor.id
            },
            success: function (response) {
              app.profesor = respuesta;
            }
          });
        },
        formatMin(diferencia) {
          if (!diferencia) return "0 hrs";
          horas = Math.floor(diferencia / 60);
          minutos = diferencia % 60;
          mensaje = "";
          if (horas > 0) {
            if (horas == 1) mensaje = horas + " hr ";
            else mensaje = horas + " hr ";
          }

          if (minutos > 0) {
            if (mensaje) mensaje += "con " + minutos + " min";
            else mensaje = minutos + " min";
          }

          return mensaje;
        },
        getProfesores() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "profesores"
            },
            success: function (response) {
              profesores = JSON.parse(response);
              for (const profesor of profesores) {
                profesor['status'] = app.definirStatus(profesor);
              }
              app.profesores = profesores;
            }
          });
        },
        getProfesor(profesor) {
          this.profesor = Object.assign({}, profesor);
          this.pagina = 2;
          //this.getDatosProfe();

          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "plantillaProfe",
              is_profe: '',
              id_profesor: profesor.id,
            },
            success: function (response) {
              respuesta = JSON.parse(response);
              app.horarios = respuesta.horarios;
              app.platilla.id = respuesta.platilla.id;
              app.platilla.estado = respuesta.platilla.estado;

            }
          });
        },
        getDia(numero) {
          if (numero == 1) return "Lunes";
          if (numero == 2) return "Martes";
          if (numero == 3) return "Miércoles";
          if (numero == 4) return "Jueves";
          if (numero == 5) return "Viernes";
          if (numero == 6) return "Sábado";
        },
        getPeriodos() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "getperiodos"
            },
            success: function (response) {
              respuesta = JSON.parse(response);
              app.periodos = respuesta.periodos;
              app.periodoActual = respuesta.actual;
            }
          });
        },
        savePeriodo() {
          mensaje = "";
          if (this.modoPeriodo == 1) {
            mensaje = 'Periodo agregado';
            data = {
              servicio: "postperiodo",
              nombre: this.periodo
            };
          }

          if (this.modoPeriodo == 2) {
            mensaje = 'Cambios guardados';
            data = {
              servicio: "putperiodo",
              nombre: this.periodo,
              id_periodo: this.auxidPeriodo
            };
          }
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: data,
            success: function (response) {
              app.getPeriodos()
              $.notify(
                mensaje, {
                  position: "right top",
                  className: "success"
                }
              );
              $('#agregarPeriodo').modal("hide");
            }
          });
        },
        setPeriodo(id, status) {
          if (status == 1) mensaje = "Periodo activado";
          else mensaje = "Periodo desactivado";
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "setperiodo",
              status: status,
              id_periodo: id
            },
            success: function (response) {
              $.notify(
                mensaje, {
                  position: "right top",
                  className: "success"
                }
              );
              app.getPeriodos()
              app.getProfesores();
            }
          });
        },
        deletePeriodo(id) {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "deleteperiodo",
              id_periodo: id
            },
            success: function (response) {
              respuesta = JSON.parse(response);



              if (!respuesta.success) {
                $.notify(
                  respuesta.mensaje, {
                    position: "right top",
                    className: "error"
                  }
                );
              } else {
                $.notify(
                  "Periodo eliminado", {
                    position: "right top",
                    className: "success"
                  }
                );
                app.getPeriodos();
              }
            }
          });
        },
        editarPeriodo(periodo) {
          this.auxidPeriodo = periodo.id;
          this.periodo = periodo.nombre;
          this.modoPeriodo = 2;
        },
        formatMin(diferencia) {
          if (!diferencia) return "0 minutos";
          horas = Math.floor(diferencia / 60);
          minutos = diferencia % 60;
          mensaje = "";
          if (horas > 0) {
            if (horas == 1) mensaje = horas + " hora ";
            else mensaje = horas + " horas ";
          }

          if (minutos > 0) {
            if (mensaje) mensaje += "con " + minutos + " minutos";
            else mensaje = minutos + " minutos";
          }

          return mensaje;
        },
        quitarClaseEdit(index) {

          clase = this.horarioSeleccionado.semana[index];
          this.horarioSeleccionado.total -= parseInt(clase.tiempo);

          this.horarioSeleccionado.porcentaje = ((this.horarioSeleccionado.total * 100) / parseInt(this
              .horarioSeleccionado.minutos))
            .toFixed(2);

          this.horarioSeleccionado.semana.splice(index, 1)
        },
        agregarClaseEdit() {

          if (this.clase.dia == -1) {
            $.notify(
              "Seleccione un lugar", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }

          if (!this.clase.lugar.id) {
            $.notify(
              "Seleccione un lugar", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }

          if (this.horaEntrada == -1) {
            $.notify(
              "Seleccione hora de entrada", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }

          if (this.horaSalida == -1) {
            $.notify(
              "Seleccione hora de salida", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }




          var clase = {
            l: this.clase.lugar.id,
            lugar: this.clase.lugar.nombre,
            d: this.clase.dia,
            e: this.horas[this.horaEntrada],
            s: this.horas[this.horaSalida],
            tiempo: this.getMin(this.horas[this.horaSalida]) - this.getMin(this.horas[this.horaEntrada]),
            sigla: this.clase.lugar.sigla
          };
          this.clase.dia = -1;
          this.horaEntrada = -1;
          this.horaSalida = -1;
          this.clase.lugar = Object.assign({}, this.lugarNull);
          this.horarioSeleccionado.total += clase.tiempo;
          this.horarioSeleccionado.porcentaje = ((this.horarioSeleccionado.total * 100) / parseInt(this
              .horarioSeleccionado.minutos))
            .toFixed(2);

          this.horarioSeleccionado.semana.push(clase);

          if (parseInt(this.horarioSeleccionado.porcentaje) == 100) {
            $.notify(
              "Tiempo completado", {
                position: "right top",
                className: "info"
              }
            );
          }

        },
        toHoras(horaSalida, horaEntrada) {
          var diferencia = horaSalida - horaEntrada;

          horas = Math.floor((diferencia * 30) / 60);
          minutos = (diferencia * 30) % 60;
          mensaje = "";
          if (horas > 0) {
            if (horas == 1) mensaje = horas + " hora ";
            else mensaje = horas + " horas ";
          }

          if (minutos > 0) {
            if (mensaje) mensaje += "con " + minutos + " minutos";
            else mensaje = minutos + " minutos";
          }

          return mensaje;
        },
        getMin(hora) {
          data = hora.split(':');
          h = parseInt(data[0]);
          m = parseInt(data[1]);
          return (h * 60) + m;
        },
        editarHorario() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "putHorario",
              id_horario: this.horarioSeleccionado.id,
              semana: this.horarioSeleccionado.semana
            },
            success: function (response) {
              app.getProfesor(app.profesor)
              $.notify(
                "Cambios guardados", {
                  position: "right top",
                  className: "success"
                }
              );
              $('#editarHorario').modal('hide');

            },
            getTiposUsuario() {
              this.userAux = Object.assign({}, this.profesor);
              $.ajax({
                type: "POST",
                url: 'controladores/api.php',
                data: {
                  servicio: "getTipos"
                },
                success: function (response) {
                  respuesta = JSON.parse(response);
                  app.tiposUsuario = respuesta;
                }
              });
            }
          });
        }
      },
      computed: {
        filtro: function () {
          if (this.buscador === '') {
            return this.profesores;
          } else {
            return this.profesores.filter((profesor) => {
              return profesor.nombre.toString().toLowerCase().includes(this.buscador.toLowerCase()) +
                profesor.materno.toString().toLowerCase().includes(this.buscador.toLowerCase()) +
                profesor.paterno.toString().toLowerCase().includes(this.buscador.toLowerCase());
            });
          }
        }

      }
    });
  </script>
</body>

</html>