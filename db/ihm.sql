﻿create table tipo_profesores(
  id int primary key,
  nombre varchar(60),
  minH int,
  maxH int
);


insert into tipo_profesores(id,nombre,minH,maxH) values(1,"Tiempo completo",10,30);
insert into tipo_profesores(id,nombre,minH,maxH) values(2,"Medio tiempo",10,28); -- Mas de 18 horas, ya son pagadas
insert into tipo_profesores(id,nombre,minH,maxH) values(3,"Investigadores de tiempo completo",8,12);
insert into tipo_profesores(id,nombre,minH,maxH) values(4,"Beca PRODET",18,40);
insert into tipo_profesores(id,nombre,minH,maxH) values(5,"Personal de asignatura sin carga administrativa",null,18);
insert into tipo_profesores(id,nombre,minH,maxH) values(6,"Personal de asignatura con carga administrativa",null,10);
insert into tipo_profesores(id,nombre,minH,maxH) values(7,"Personal sindicalizado",null,15);
insert into tipo_profesores(id,nombre,minH,maxH) values(8,"Plaza de coordinadores",null,10);
insert into tipo_profesores(id,nombre,minH,maxH) values(9,"Personal de autoacceso",null,30);


create table usuarios( 
  id int primary key auto_increment, 
  rfc varchar(13) not null, 
  email varchar(50) not null unique, 
  oficina varchar(10), 
  celular varchar(10) not null, 
  doc_grado varchar(130), 
  tipo_usuario TINYINT default 1, 
  password varchar(35) not null, 
  nombre varchar(30) not null, 
  paterno varchar(30) not null, 
  materno varchar(30) not null, 
  id_tipo_profesor int, 
  foreign key(id_tipo_profesor) references tipo_profesores(id)
);




insert into usuarios
(rfc,email,celular,tipo_usuario,pass,nombre,paterno,materno) 
values
('123456789','administrador@gmail.com','7121397374',2,'123456','administrador','p','m');



insert into usuarios
(rfc,email,celular,password,nombre,paterno,materno,id_tipo_profesor) 
values
('123456789','profe1@gmail.com','7121397374','123456','Profe1','Paterno1','Materno1',1);



insert into usuarios
(rfc,email,celular,password,nombre,paterno,materno,id_tipo_profesor) 
values
('123456789','profe2@gmail.com','7121397374','123456','Profe2','Paterno2','Materno2',2);


insert into usuarios
(rfc,email,celular,password,nombre,paterno,materno,id_tipo_profesor) 
values
('123456789','jovannyrch@gmail.com','7121397374','123456','Jovanny','Rammírez','Chimal',3);


insert into usuarios
(rfc,email,celular,password,nombre,paterno,materno,id_tipo_profesor) 
values
('123456789','profe4@gmail.com','7121397374','123456','Profe3','Paterno3','Materno3',6);




create table periodos(
  id int primary key not null auto_increment,
  nombre varchar(30) not null,
  actual boolean default 0 not null
);

insert into periodos(nombre,actual) values("2019B",1);


create table planes(
  id int primary key,
  nombre varchar(30) not null
);

insert into planes(id,nombre) values(1,'F2');
insert into planes(id,nombre) values(2,'F19');

create table materias(
  id int primary key not null auto_increment,
  id_plan int not null, 
  foreign key (id_plan) references planes(id),
  nombre varchar(50),
  minutos int
);


insert into materias(id_plan,nombre,minutos) values(1,'Programación Estructurada',360);
insert into materias(id_plan,nombre,minutos) values(1,'Lógica',180);
insert into materias(id_plan,nombre,minutos) values(1,'Programación Avanzada',360);
insert into materias(id_plan,nombre,minutos) values(1,'Estructura de Datos',300);
insert into materias(id_plan,nombre,minutos) values(1,'Física Básica',270);
insert into materias(id_plan,nombre,minutos) values(1,'Automátas y Lenguajes Formales',180);
insert into materias(id_plan,nombre,minutos) values(1,'Investigación de Operaciones',240);
insert into materias(id_plan,nombre,minutos) values(1,'Metrología',180);
insert into materias(id_plan,nombre,minutos) values(1,'Electricidad y Magnetismo',300);
insert into materias(id_plan,nombre,minutos) values(1,'Organización de Archivos',300);
insert into materias(id_plan,nombre,minutos) values(1,'Lenguaje Ensamblador',120);
insert into materias(id_plan,nombre,minutos) values(1,'Programación Orientada a Objetos',300);
insert into materias(id_plan,nombre,minutos) values(1,'Lenguaje de Programación Estructurado',180);
insert into materias(id_plan,nombre,minutos) values(1,'Comunicación y Relaciones Humanas',180);
insert into materias(id_plan,nombre,minutos) values(1,'Circuitos Eléctricos',300);
insert into materias(id_plan,nombre,minutos) values(1,'Transmición de Datos',300);
insert into materias(id_plan,nombre,minutos) values(1,'Fundamentos de Base de Datos',240);
insert into materias(id_plan,nombre,minutos) values(1,'Ensambladores',240);
insert into materias(id_plan,nombre,minutos) values(1,'Programación Paralela y Distribuida',300);
insert into materias(id_plan,nombre,minutos) values(1,'Legislación Informática',120);
insert into materias(id_plan,nombre,minutos) values(1,'Lenguaje de Programación Visual',180);
insert into materias(id_plan,nombre,minutos) values(1,'Economía',120);
insert into materias(id_plan,nombre,minutos) values(1,'Electrónica Analógica',300);
insert into materias(id_plan,nombre,minutos) values(1,'Protocolos de Red',300);
insert into materias(id_plan,nombre,minutos) values(1,'Base de Datos Avanzados',240);
insert into materias(id_plan,nombre,minutos) values(1,'Compiladores',300);
insert into materias(id_plan,nombre,minutos) values(1,'Sistemas Expertos',180);
insert into materias(id_plan,nombre,minutos) values(1,'Formación de Lideres',180);
insert into materias(id_plan,nombre,minutos) values(1,'Servicios de Internet',180);
insert into materias(id_plan,nombre,minutos) values(1,'Multimedia',240);
insert into materias(id_plan,nombre,minutos) values(1,'Desarrollo Multimedia',180);
insert into materias(id_plan,nombre,minutos) values(1,'Administración',180);
insert into materias(id_plan,nombre,minutos) values(1,'Electrónica Digital',300);
insert into materias(id_plan,nombre,minutos) values(1,'Modelos de Redes',240);
insert into materias(id_plan,nombre,minutos) values(1,'Sistemas Gestores de Bases de Datos',240);
insert into materias(id_plan,nombre,minutos) values(1,'Sistemas Operativos',300);
insert into materias(id_plan,nombre,minutos) values(1,'Fundamentos de Robótica',180);
insert into materias(id_plan,nombre,minutos) values(1,'Estándares de Calidad',240);
insert into materias(id_plan,nombre,minutos) values(1,'Tipos y Configuraciones',180);
insert into materias(id_plan,nombre,minutos) values(1,'Redes Neuronales',180);
insert into materias(id_plan,nombre,minutos) values(1,'Lenguaje de Programación Orientado a Objetos',180);
insert into materias(id_plan,nombre,minutos) values(1,'Administración de Recursos Informáticos',180);
insert into materias(id_plan,nombre,minutos) values(1,'Lógica Secuencial y Combinatoria',360);
insert into materias(id_plan,nombre,minutos) values(1,'Administración de Redes',300);
insert into materias(id_plan,nombre,minutos) values(1,'Teoría de Sistemas',180);
insert into materias(id_plan,nombre,minutos) values(1,'Tipos de Sistemas Operativos',300);
insert into materias(id_plan,nombre,minutos) values(1,'Tratamiento de Imágenes',180);
insert into materias(id_plan,nombre,minutos) values(1,'Métricas de Software',240);
insert into materias(id_plan,nombre,minutos) values(1,'Instalaciones y Equipos',180);
insert into materias(id_plan,nombre,minutos) values(1,'Algoritmos Genéticos',180);
insert into materias(id_plan,nombre,minutos) values(1,'Minería	de	Datos',240);
insert into materias(id_plan,nombre,minutos) values(1,'Metodología de la Investigación',180);
insert into materias(id_plan,nombre,minutos) values(1,'Sistemas	Digitales',360);
insert into materias(id_plan,nombre,minutos) values(1,'Análisis	y	Diseño de Redes',180);
insert into materias(id_plan,nombre,minutos) values(1,'Análisis	de	Sistemas',300);
insert into materias(id_plan,nombre,minutos) values(1,'Graficación',180);
insert into materias(id_plan,nombre,minutos) values(1,'Auditoría Informática',240);
insert into materias(id_plan,nombre,minutos) values(1,'Interconexión	de	Redes',180);
insert into materias(id_plan,nombre,minutos) values(1,'Visión	Artificial',120);
insert into materias(id_plan,nombre,minutos) values(1,'Métricas	de	Software',120);
insert into materias(id_plan,nombre,minutos) values(1,'Arquitectura	de Computadoras',300);
insert into materias(id_plan,nombre,minutos) values(1,'Seguridad en Redes',300);
insert into materias(id_plan,nombre,minutos) values(1,'Diseño	de	Sistemas',300);
insert into materias(id_plan,nombre,minutos) values(1,'Instalación,	Configuración	y	Comunicación de Sistemas Operativos',300);
insert into materias(id_plan,nombre,minutos) values(1,'Interacción	HombreMáquina',180);
insert into materias(id_plan,nombre,minutos) values(1,'Administración	de Proyectos Informáticos',240);
insert into materias(id_plan,nombre,minutos) values(1,'Auditoría de Redes',180);
insert into materias(id_plan,nombre,minutos) values(1,'Robótica	Avanzada',180);
insert into materias(id_plan,nombre,minutos) values(1,'Análisis	de Fourier',120);
insert into materias(id_plan,nombre,minutos) values(1,'Cálculo numérico',180);
insert into materias(id_plan,nombre,minutos) values(1,'Simulación',180);
insert into materias(id_plan,nombre,minutos) values(1,'Inferencia Estadística',180);
insert into materias(id_plan,nombre,minutos) values(1,'Variable Compleja',180);
insert into materias(id_plan,nombre,minutos) values(1,'Ética',120);
insert into materias(id_plan,nombre,minutos) values(1,'Sociedad e Ingeniería',120);
insert into materias(id_plan,nombre,minutos) values(1,'Lectura y Redacción',120);
insert into materias(id_plan,nombre,minutos) values(1,'Química General',240);
insert into materias(id_plan,nombre,minutos) values(1,'Sociología',120);
insert into materias(id_plan,nombre,minutos) values(1,'Análisis y Diseño de Redes',300);
insert into materias(id_plan,nombre,minutos) values(1,'Administración	y Seguridad de Sistemas Operativos',180);
insert into materias(id_plan,nombre,minutos) values(1,'Análisis de Lenguajes de Programación ',240);


insert into materias(id_plan,nombre,minutos) values(2,'Programación I',240);
insert into materias(id_plan,nombre,minutos) values(2,'El Ingeniero y su Entorno Socieconómico',240);
insert into materias(id_plan,nombre,minutos) values(2,'Epistemología',240);
insert into materias(id_plan,nombre,minutos) values(2,'Programación II',240);
insert into materias(id_plan,nombre,minutos) values(2,'Química',240);
insert into materias(id_plan,nombre,minutos) values(2,'Comunicación y Relaciones Humanas',240);
insert into materias(id_plan,nombre,minutos) values(2,'Física',240);
insert into materias(id_plan,nombre,minutos) values(2,'Paradigmas y Programación I',240);
insert into materias(id_plan,nombre,minutos) values(2,'Base de Datos I',240);
insert into materias(id_plan,nombre,minutos) values(2,'Arquitectura de Computadoras',240);
insert into materias(id_plan,nombre,minutos) values(2,'Métodos estadísticos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Métodos numéricos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Paradigmas de Programación II',240);
insert into materias(id_plan,nombre,minutos) values(2,'Base de Datos II',240);
insert into materias(id_plan,nombre,minutos) values(2,'Electromagnetismo',240);
insert into materias(id_plan,nombre,minutos) values(2,'Circuitos eléctricos y electrónicos',360);
insert into materias(id_plan,nombre,minutos) values(2,'Transmisión de Datos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Investigación de Operaciones',240);
insert into materias(id_plan,nombre,minutos) values(2,'Ingeniería de Software I',240);
insert into materias(id_plan,nombre,minutos) values(2,'Ensambladores',240);
insert into materias(id_plan,nombre,minutos) values(2,'Inteligencia Artificial',240);
insert into materias(id_plan,nombre,minutos) values(2,'Sistemas Analógicos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Protocolos de Comunicación de Datos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Administración de Recursos Informáticos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Ingeniería de Software II',240);
insert into materias(id_plan,nombre,minutos) values(2,'Compiladores',240);
insert into materias(id_plan,nombre,minutos) values(2,'Procesamiento de Imágenes Digitales',240);
insert into materias(id_plan,nombre,minutos) values(2,'Sistemas Digitales',240);
insert into materias(id_plan,nombre,minutos) values(2,'Arquitectura de Redes',240);
insert into materias(id_plan,nombre,minutos) values(2,'Administración de Proyectos Informáticos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Ciencia de los Datos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Sistemas Operativos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Róbotica',240);
insert into materias(id_plan,nombre,minutos) values(2,'Graficación Computacional',240);
insert into materias(id_plan,nombre,minutos) values(2,'Sistemas Embebidos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Seguridad de la Información',240);
insert into materias(id_plan,nombre,minutos) values(2,'Gestión de Proyectos de Investigación',240);
insert into materias(id_plan,nombre,minutos) values(2,'Tecnologías Computacionales I',240);
insert into materias(id_plan,nombre,minutos) values(2,'Integrativa Profesional',240);
insert into materias(id_plan,nombre,minutos) values(2,'Ética Profesional y Sustentabilidad',240);
insert into materias(id_plan,nombre,minutos) values(2,'Proyecto Integral de Comunicación de Datos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Proyecto Integral de Ingeniería de Software',240);
insert into materias(id_plan,nombre,minutos) values(2,'Tecnologías Computacionales II',240);
insert into materias(id_plan,nombre,minutos) values(2,'Práctica Profesional',240);
insert into materias(id_plan,nombre,minutos) values(2,'Análisis y Diseño de Redes',240);
insert into materias(id_plan,nombre,minutos) values(2,'Visión Artificial',240);
insert into materias(id_plan,nombre,minutos) values(2,'Reconocimiento de Patrones',240);
insert into materias(id_plan,nombre,minutos) values(2,'Gestión de Redes',240);
insert into materias(id_plan,nombre,minutos) values(2,'Computing in Industrial',240);
insert into materias(id_plan,nombre,minutos) values(2,'Interacción Hombre-Maquina',240);
insert into materias(id_plan,nombre,minutos) values(2,'Tecnologías emergentes',240);
insert into materias(id_plan,nombre,minutos) values(2,'Tópicos de Tecnologías de Datos',240);
insert into materias(id_plan,nombre,minutos) values(2,'Sistemas Interactivos',240);



alter table usuarios add column status TINYINT default 0;


create or replace table validacion(
	id_profesor int not null,
	foreign key (id_profesor) references usuarios(id),
	verificado int(11) NOT NULL COMMENT '0=no, 1=yes',
	codigo_verificacion varchar(264) NOT NULL primary key,
	tipo_verificacion int(10) NOT NULL COMMENT '0=correo, 1=celular',
	fecha_creacion datetime NOT NULL,
	fecha_modificacion timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);



create or replace table lugares(
  id int UNSIGNED primary key auto_increment,
  nombre varchar(50),
  sigla varchar(5)
);

insert into lugares(nombre,sigla) values('Aula','AU');
insert into lugares(nombre,sigla) values('Sala de Cómputo','SC');
insert into lugares(nombre,sigla) values('Laboratorio de Cómputo Paralelo','LC');
insert into lugares(nombre,sigla) values('Laboratorio de Electrónica','LE');
insert into lugares(nombre,sigla) values('Laboratorio de Física','LF');
insert into lugares(nombre,sigla) values('Laboratorio de Interacción Hombre-Máquina','LI');
insert into lugares(nombre,sigla) values('Laboratorio de Redes','LRe');
insert into lugares(nombre,sigla) values('Laboratorio de Robótica','LRo');

create or replace table horarios(
  id int primary key auto_increment not null,
  id_materia int not null,
  foreign key (id_materia) references materias(id),
  id_periodo int not null,
  foreign key (id_periodo) references periodos(id),
  id_profesor int not null,
  foreign key (id_profesor) references usuarios(id) on delete cascade,
  status TINYINT default 0
);


create or replace table clases(
  id int UNSIGNED primary key auto_increment,
  id_horario int not null,
  foreign key (id_horario) references horarios(id) on delete cascade,
  dia int(1) not null,
  he varchar(5) not null,
  hs varchar(5) not null,
  id_lugar int UNSIGNED not null,
  foreign key (id_lugar) references lugares(id),
  tiempo int not null default 0
);


create table plantillas(
  id int primary key not null auto_increment,
  id_profesor int,
  foreign key (id_profesor) references usuarios(id),
  id_periodo int,
  foreign key (id_periodo) references periodos(id),
  estado TINYINT COMMENT '0=EL_PROFE_PROPONE_EL_HORARIO,1=LO ENVIO PROFESOR, 2=NO_HUBO_MODIFICACIONES_ADMISTRADOR_ACEPTO, SI HAY MODIFICACIONES REGRESA A 0'
);

alter table horarios add column id_plantilla int default null;
alter table horarios add foreign key (id_plantilla) references plantillas(id);



827ccb0eea8a706c4c34a16891f84e7b


