<?php
  require 'modelos/inicio_profesor.php';
?>


<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Inicio</title>

  <?php require 'partials/archivos.php' ?>

</head>

<body>
  <div class="container bg-white" id="app">

    <?php require 'partials/header.php' ?>
    <div>

      <div class="">
        <h2 class="verde-oro text-light p-3">Registro de plantilla</h2> <span class="pull-right">
          <a href="logout.php"> <i class="fa fa-sign-out"></i>
            Cerrar sesión</a></span>
        <br>

      </div>
      <br><br>
      <div class="row">
        <div class="card col-12 col-lg-3 border border-success">
          <div class="card-body">
            <div class="border pb-4 p-1  pt-2 row">

              <h6 class=" text-verde"><b>Datos personales</b></h6>
              <div class="linea-ov"></div>
              <div class="mt-2">
                <small>
                    <b>Nombre:</b> <br> {{usuario.nombre}} {{usuario.paterno}} {{usuario.materno}} <br>
                    <b>Email:</b> <br>{{usuario.email}} <br>
    
                    <b>Celular:</b> <br> {{usuario.celular}} <br>
                </small>
              </div> <br>

              <br>

              <small class=" col-12">
                <a href="#" class="text-oro" data-toggle="modal" data-target="#visualizador">
                  Ver archivo comprobante de estudios
                </a>
              </small>

              <span class=" col-12">
                <small><a href="#" class="text-oro" data-toggle="modal" data-target="#cambioPass"
                    @click="pass.actual = ''; pass.nueva = ''; pass.nueva2 = ''">Cambiar contraseña</a></small>
              </span>


              <span class=" col-12"><button class="btn btn-sm text-oro pull-right" data-toggle="modal"
                  @click="getTiposUsuario()" data-target="#editarDatos"> <i class="fa fa-pencil"></i> Editar Datos
                </button>
              </span>
            </div>



            <div class="border col-12 mt-2 pt-2 row">
              <div class="mb-1">
                <small>
                  <h6 class="text-verde"><b>Tipo de docente:</b></h6>

                  <div class="linea-ov mb-1"></div>
                  {{usuario.tipo_profesor}} <br>
                </small>
              </div>
            </div>

            <div class="border col-12 mt-2 pt-1 row">
              <small class="pb-1">
                <h6 class="text-verde"><b>Horas laborales</b></h6>
                <div class="linea-ov mb-1"></div>
                <div v-if="usuario.minH" class="mb-1">
                  <b>Mínimo:</b> <br>
                  {{formatMin(usuario.minH*60)}}
                </div>
                <div v-if="usuario.maxH">

                  <b>Máximo:</b> <br>
                  {{formatMin(usuario.maxH*60)}}
                </div>
              </small>
            </div>

            <div class="border col-12 mt-2 row">
              <small class="pb-1">
                <h6 class="text-verde"> <b>Tiempo acomulado:</b> </h6>
                <div class="linea-ov mb-1"></div>
                {{formatMin(tiempo_usuario)}}
              </small>
            </div>


          </div>



        </div>




        <div class="card col-12 col-lg-9 border border-success">
          <div class="card-body">
            <h5 class="text-verde text-center"><b>Plantilla de horarios {{periodoActual.nombre}}</b></h5>
            <div class="linea-vo mb-3">
            </div>
            <div class="col-12 mb-4 container">
              <button type="button" v-if='!plantilla.estado' class="btn pull-right btn-sm verde" @click="nuevaMateria"
                data-toggle="modal" data-target="#agregarMateria">
                Agregar materia
              </button> <br>
              <br>
              <button v-if='!plantilla.estado && horarios.length' data-toggle="modal" data-target="#confirmacionEnviar"  :class="horarios.length? 'btn btn-primary btn-sm pull-right mb-3':'btn btn-default pull-right mb-3'" >Enviar
                plantilla</button>
              

              <span v-if="plantilla.estado == 0" class="pull-right mb-4 alert alert-warning">
                Plantilla en revisión
              </span>

            </div>
            <br>
            <div :class="statusProfe.clase+' mt-2'">
              <small>
                <span v-html="statusProfe.mensaje"></span>
              </small>
            </div>

          <!--   <div class="form-group" v-if="horarios.length && horarios.length > 1">
              <label for="materiasProfe" class="text-verde"><b>Ver horario(s) Materia</b></label>
              <select class="form-control" name="materiasProfe" id="materiasProfe" v-model="materiaProfeSeleccionada"
                @change="getHorarios">
                <option value="">Todas las materias</option>
                <option :value="m.id" v-for="m in materiasProfe">{{m.nombre}} ({{m.plan}})</option>
              </select>
            </div>
-->

            <div
              :class="h.status == 2 ?'card col-12 mt-2 d-none d-lg-block alert-verde-1':'card col-12 mt-2 d-none d-lg-block'"
              v-for="h in horarios" v-if="horarios.length">
              <div class="card-body row">

                <div class="col-12">
                  <div class="col-12 pull-right mb-2" >
                    <button @click="selecionarHorario(h)" v-if="!plantilla.id" style="background-color: rgb(189, 65, 71); color: white"
                      class="btn pull-right btn-sm" data-toggle="modal" data-target="#confirmarEliminacion"> <i
                        class="fa fa-trash"></i> </button>

                    <button @click="selecionarHorario(h)" v-if="h.status == 0" style="background-color: rgb(190, 192, 66); color: white"
                      class="btn pull-right btn-sm mr-1" data-toggle="modal" data-target="#editarHorario"> <i
                        class="fa fa-edit"></i> </button>
                  </div>
                  
                  <div  class="col-12 pull-right mb-1 text-center">
                    <div v-if="h.status == 1 || h.status == 2" class="alert alert-verde-pastel alert-dismissible fade show" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <strong v-if="h.status == 1" >Horario aprobado por la administración</strong>
                      <strong v-if="h.status == 2" >Horario fijado</strong>
                    </div>

                    <button v-if="h.status == 0 && plantilla.id" @click="setStatusH(3,h.id)" style=" background-color: rgb(30, 112, 219);
                    color: white" class="btn pull-right btn-sm mr-1">
                    <i class="fa fa-send" aria-hidden="true"></i> Enviar nueva propuesta
                  </button>



                    <button v-if="h.status == 1 && h.status != 2" @click="setStatusH(0,h.id)" style=" background-color: rgb(219, 30, 30);
                      color: white" class="btn pull-right btn-sm mr-1">
                      <i class="fa fa-times" aria-hidden="true"></i> Rechazar
                    </button>

                    <button v-if="h.status == 1 && h.status != 2" @click="setStatusH(2,h.id)" style="  background-color: rgb(34, 151, 87);
                    color: white" class="btn pull-right btn-sm mr-1">
                      <i class="fa fa-check" aria-hidden="true"></i> Aceptar
                    </button>

                  </div>

                  <h5 class="text-verde"><b>{{h.materia}} ({{h.plan}})</b></h5>
                  <div class="linea-vo">
                  </div>


                  <div class="d-flex flex-row pt-3 col-12 hidden-sm-down" v-if="h.semana.length">
                    <div v-for="dia in h.semana" v-if="dia.e && dia.s" class=" border   mr-1 bg-white">
                      <span class="form-control text-center mb-auto"
                        style="background-color: #9C8412; color: white">{{getDia(dia.d)}}</span>
                      <p class=" text-center"><i v-if="dia.e && dia.s"> <i class="fa fa-clock-o" aria-hidden="true"></i>
                          <br> {{dia.e}} - {{dia.s}}</i> </p>
                      <p class=" text-center " data-toggle="tooltip" data-placement="top" :title="dia.lugar">
                        <i class="fa fa-map-marker" aria-hidden="true"></i> <br> {{dia.sigla}}
                      </p>
                    </div>
                  </div>

                  <div v-if="!h.semana.length">
                    <div class="alert " role="alert">
                      <h6 class="alert-heading text-center">Agrege clases a la materia</h6>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 pt-3">
                      <h6 class="text-verde">Total horas: {{formatMin(h.total)}}</h6>
                    </div>
                  </div>
                </div>
              </div>
              <br>
            </div>



            <div class="d-sm-block  d-lg-none d-xl-none d-sm-none">
              <div :class="h.status==1? 'card mb-4 alert-verde-1':'card mb-4'" v-for="h in horarios">
                <div class="card-body">

                  <div class="col-12 pull-right">
                    <button @click="selecionarHorario(h)" style="background-color: rgb(189, 65, 71); color: white"
                      class="btn pull-right btn-sm" data-toggle="modal" data-target="#confirmarEliminacion"> <i
                        class="fa fa-trash"></i> </button>
                    <button @click="selecionarHorario(h)" style="background-color: rgb(190, 192, 66); color: white"
                      class="btn pull-right btn-sm mr-1" data-toggle="modal" data-target="#editarHorario"> <i
                        class="fa fa-edit"></i> </button>
                  </div>

                  <h4 class="text-verde"> <b>{{h.materia}} ({{h.plan}})</b></h4>
                  <div class="linea-vo">
                  </div>
                </div>
                <ul :class="h.status==1? 'list-group list-group-flush alert-verde-1':'list-group list-group-flush'">
                  <li class="list-group-item" v-for="dia in h.semana" v-if="dia.e && dia.s">
                    {{getDia(dia.d)}} / <span><i>{{dia.e}} - {{dia.s}} / {{dia.lugar}}</i></span>


                  </li>
                  <li class="list-group-item">
                    <div class="row">
                      <div class="col-12">
                        <div class="alert alert-dark pull-right m-0 p-1" role="alert"
                          style="width: 40%;margin-top: 0%; padding-top: 0%">
                          <small>Tiempo acomulado: {{formatMin(h.total)}}</small>
                        </div>
                      </div>


                    </div>
                  </li>
                </ul>
              </div>
            </div>



            <div v-if="!horarios.length">
              <div class="alert" style="background-color: rgb(195, 212, 235)" role="alert">
                <h6 class="alert-heading text-center">Sin registros, pulse el botón "Agregar materia" y luego "Enviar plantilla" cuando haya llenado su plantilla</h6>
              </div>
            </div>

          </div>



        </div>
      </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="editarHorario" tabindex="-1" role="dialog" aria-labelledby="editH" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Editar horario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container-fluid">

              <div class="row">
                <div :class="horarioSeleccionado.porcentaje == 100? 'col-12':'col-8'">

                  <div class="d-flex flex-row pt-3 col-12 hidden-sm-down" v-if="horarioSeleccionado.semana.length">
                    <div v-for="(dia,index) in horarioSeleccionado.semana" class="border p-1 mr-1">
                      <span class="form-control text-center mb-auto"
                        style="background-color: #9C8412; color: white">{{getDia(dia.d)}}</span>
                      <p class=" text-center"><i v-if="dia.e && dia.s">{{dia.e}} - {{dia.s}}</i> </p>
                      <p class=" text-center border">{{dia.sigla}}</p>

                      <button @click="quitarClaseEdit(index)" class="btn  btn-sm border pull-right"
                        style="background-color: rgb(218, 216, 210)"> <i class="fa fa-times"></i> </button>

                    </div>

                  </div>

                  <div v-if="!horarioSeleccionado.semana.length" style="height: 50%;">
                    <div class="alert mt-4" role="alert">
                      <h6 class="alert-heading text-center">Agrege clases a la materia</h6>
                    </div>
                  </div>

                  <div class="row">
                    <div class="progress col-12 mt-3 mb-3 ">
                      <div class="progress-bar bg-success" role="progressbar"
                        :style="'width: '+horarioSeleccionado.porcentaje+'%'" aria-valuenow="25" aria-valuemin="0"
                        aria-valuemax="100">
                        {{horarioSeleccionado.porcentaje}}%</div>
                    </div>



                    <div class="col-12">
                      <div class="alert alert-dark pull-right m-0 p-0" role="alert"
                        style="width: 40%;margin-top: 0%; padding-top: 0%">
                        <small>Tiempo acomulado: {{formatMin(horarioSeleccionado.total)}}</small>
                      </div>
                    </div>

                    <div class="col-12">
                      <div :class="getClase(controlMateria.porcentaje)" role="alert"
                        style="width: 40%;margin-top: 0%; padding-top: 0%">
                        <small>Tiempo restante:
                          {{formatMin(parseInt(horarioSeleccionado.minutos) - horarioSeleccionado.total)}}</small>
                      </div>
                    </div>

                  </div>


                </div>

                <div class="col-md-4" v-if="horarioSeleccionado.porcentaje != 100">

                  <div class="form-group col-12">
                    <label for="diaS">Día</label>
                    <select class="form-control" name="diaS" id="diaS" v-model="clase.dia">
                      <option selected value="-1" disabled>Seleccione un día</option>
                      <option value="1">Lunes</option>
                      <option value="2">Martes</option>
                      <option value="3">Miércoles</option>
                      <option value="4">Jueves</option>
                      <option value="5">Viernes</option>
                      <option value="6">Sábado</option>
                    </select>
                  </div>

                  <div class="form-group col-12">
                    <label for="lugars">Lugar</label>
                    <select class="custom-select" name="lugars" id="lugars" v-model="clase.lugar">
                      <option selected disabled :value="lugarNull">Seleccione lugar</option>
                      <option :value="l" v-for='l in lugares'>{{l.nombre}}</option>
                    </select>
                  </div>


                  <div class="form-group  col-12">
                    <label for="he">Hora de entrada</label>
                    <select class="form-control" name="he" id="he" v-model="horaEntrada">
                      <option selected value="-1" disabled>Seleccione hora de entrada</option>
                      <option v-for="i in (horas.length-1)" :value="i-1">{{horas[i-1]}} </option>
                    </select>
                  </div>
                  <div class="form-group  col-12" v-if="horaEntrada != -1">
                    <label for="he">Hora de salida</label>
                    <select class="form-control" name="he" id="he" v-model="horaSalida">
                      <option selected value="-1" disabled>Seleccione hora de salida</option>
                      <option v-for="i in ((horarioSeleccionado.minutos-horarioSeleccionado.total)/30)"
                        :value="i+horaEntrada">
                        {{horas[i+horaEntrada]}}
                      </option>
                    </select>
                  </div>

                  <div class="alert alert-light text-center col-12" role="alert"
                    v-if="horaEntrada != -1 && horaSalida != -1">
                    <strong> Duración: {{toHoras(horaSalida,horaEntrada)}} </strong>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button v-if="horarioSeleccionado.porcentaje == 100" type="button" class="btn verde btn-sm"
              @click="editarHorario()">Guardar cambios</button>
            <button v-else type="button" class="btn verde btn-sm" @click="agregarClaseEdit">Agregar clase</button>
            <button type="button" class="btn oro btn-sm" data-dismiss="modal">Cancelar</button>

          </div>
        </div>
      </div>
    </div>





    <!-- Modal -->
    <div class="modal fade" id="agregarMateria" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
      aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 v-if="!modelMateria.id" class="modal-title text-light">Agregar materia</h5>
            <h5 v-else class="modal-title text-light">Agregar clases de {{modelMateria.nombre}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="container-fluid">

              <div v-if="!modelMateria.id">
                <br>
                <div class="form-group">
                  <label for="materia">Plan de estudio</label>
                  <select class="custom-select" v-model="planSeleccionado" name="materia" id="materia"
                    @change="getMaterias()">
                    <option selected disabled value="">Seleccione un plan</option>
                    <option :value="plan.id" v-for="plan in planes">{{plan.nombre}}</option>
                  </select>
                </div>

                <div class="form-group" v-show="materias.length" v-if="planSeleccionado">
                  <label for="materia">Unidad de aprendizaje</label>
                  <select class="form-control" name="materia" id="materia" v-model="modelMateria">
                    <option disabled selected value="-1">Selecione unidad de aprendizaje</option>
                    <option :value="materia" v-for="(materia,index) in materias">{{materia.nombre}}</option>
                  </select>
                </div>
              </div>

              <div class="row" v-else>
                <div :class="controlMateria.porcentaje == 100? 'col-12':'col-8'">

                  <div class="d-flex flex-row pt-3 col-12 hidden-sm-down" v-if="controlMateria.semana.length">
                    <div v-for="(dia,index) in controlMateria.semana" class="border p-1 mr-1">
                      <span class="form-control text-center mb-auto"
                        style="background-color: #9C8412; color: white">{{getDia(dia.d)}}</span>
                      <p class=" text-center"><i v-if="dia.e && dia.s">{{dia.e}} - {{dia.s}}</i> </p>
                      <p class=" text-center border">{{dia.lugar}}</p>

                      <button @click="quitarClase2(index)" class="btn  btn-sm border pull-right"
                        style="background-color: rgb(218, 216, 210)"> <i class="fa fa-times"></i> </button>

                    </div>

                  </div>

                  <div v-if="!controlMateria.semana.length" style="height: 50%;">
                    <div class="alert mt-4" role="alert">
                      <h6 class="alert-heading text-center">Agrege clases a la materia</h6>
                    </div>
                  </div>

                  <div class="row">
                    <div class="progress col-12 mt-3 mb-3 ">
                      <div class="progress-bar bg-success" role="progressbar"
                        :style="'width: '+controlMateria.porcentaje+'%'" aria-valuenow="25" aria-valuemin="0"
                        aria-valuemax="100">
                        {{controlMateria.porcentaje}}%</div>
                    </div>



                    <div class="col-12">
                      <div class="alert alert-dark pull-right m-0 p-0" role="alert"
                        style="width: 40%;margin-top: 0%; padding-top: 0%">
                        <small>Tiempo acomulado: {{formatMin(controlMateria.total)}}</small>
                      </div>
                    </div>

                    <div class="col-12 mt-1">
                      <div :class="getClase(controlMateria.porcentaje)" role="alert"
                        style="width: 40%;margin-top: 0%; padding-top: 0%">
                        <small>Restan: {{formatDiferencia}}</small>
                      </div>
                    </div>

                  </div>


                </div>

                <div class="col-md-4" v-if="controlMateria.porcentaje != 100">

                  <div class="form-group col-12">
                    <label for="diaS">Día</label>
                    <select class="form-control" name="diaS" id="diaS" v-model="clase.dia">
                      <option selected value="-1" disabled>Seleccione un día</option>
                      <option value="1">Lunes</option>
                      <option value="2">Martes</option>
                      <option value="3">Miércoles</option>
                      <option value="4">Jueves</option>
                      <option value="5">Viernes</option>
                      <option value="6">Sábado</option>
                    </select>
                  </div>

                  <div class="form-group col-12">
                    <label for="lugars">Lugar</label>
                    <select class="custom-select" name="lugars" id="lugars" v-model="clase.lugar">
                      <option selected disabled :value="lugarNull">Seleccione lugar</option>
                      <option :value="l" v-for='l in lugares'>{{l.nombre}}</option>
                    </select>
                  </div>


                  <div class="form-group  col-12">
                    <label for="he">Hora de entrada</label>
                    <select class="form-control" name="he" id="he" v-model="horaEntrada2">
                      <option selected value="-1" disabled>Seleccione hora de entrada</option>
                      <option v-for="i in (horas.length-1)" :value="i-1">{{horas[i-1]}} </option>
                    </select>
                  </div>

                  <div class="form-group  col-12" v-if="horaEntrada2 != -1">
                    <label for="he">Hora de salida</label>
                    <select class="form-control" name="he" id="he" v-model="horaSalida2">
                      <option selected value="-1" disabled>Seleccione hora de salida</option>
                      <option v-for="i in ((modelMateria.minutos-controlMateria.total)/30)" :value="i+horaEntrada2">
                        {{horas[i+horaEntrada2]}}
                      </option>
                    </select>
                  </div>

                  <div class="alert alert-light text-center col-12" role="alert"
                    v-if="horaEntrada2 != -1 && horaSalida2 != -1">
                    <strong> Duración: {{toHoras(horaSalida2,horaEntrada2)}} </strong>
                  </div>

                </div>
              </div>
            </div>
          </div>
          {{verificadorHora}}
          <div class="modal-footer">
            <button v-if="controlMateria.porcentaje == 100" type="button" class="btn btn-success btn-sm"
              @click="guardarMateria()">Agregar horario</button>
            <button v-else type="button" class="btn verde btn-sm" @click="agregarClase">Agregar clase</button>
            <button type="button" class="btn oro btn-sm" data-dismiss="modal">Cancelar</button>

          </div>
        </div>
      </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="confirmarEliminacion" tabindex="-1" role="dialog" aria-labelledby="sss"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Confirmar eliminación</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body centrado">
            ¿Estás seguro de eliminar el horario de la materia
            <b>{{horarioSeleccionado.materia}}</b>?
          </div>
          <div class="modal-footer ">
            <button type="button" class="btn verde  btn-sm" data-dismiss="modal"
              @click="eliminarHorario(horarioSeleccionado.id)">Eliminar</button>
            <button type="button" class="btn oro  btn-sm" data-dismiss="modal">Cancelar</button>

          </div>
        </div>
      </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="cambioPass" tabindex="-1" role="dialog" aria-labelledby="passID" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title ">Cambiar contraseña</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <div class="form-group">
              <label for="passActual">Contraseña actual</label>
              <input type="password" class="form-control" v-model="pass.actual" name="passActual" id="passActual"
                aria-describedby="helpId" placeholder="Escriba la contraseña actual">
            </div>
          </div>

          <div class="modal-body">
            <div class="form-group">
              <label for="passNueva">Contraseña nueva</label>
              <input type="password" class="form-control" v-model="pass.nueva" name="passNueva" id="passNueva"
                aria-describedby="helpId" placeholder="Escriba la nueva contraseña">
            </div>
          </div>

          <div class="modal-body">
            <div class="form-group">
              <label for="passNueva2">Confirme contraseña nueva</label>
              <input type="password" class="form-control" v-model="pass.nueva2" name="passNueva2" id="passNueva2"
                aria-describedby="helpId" placeholder="Confirme nueva contraseña">
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn verde  btn-sm"
              v-if="pass.nueva2 === pass.nueva && pass.nueva2 && pass.nueva && pass.actual"
              @click="actualizarPass">Guardar</button>
            <button type="button" class="btn oro  btn-sm" data-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="visualizador" tabindex="-1" role="dialog" aria-labelledby="viewer" aria-hidden="true">
      <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Visualizador de archivos</h5>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container-fluid" v-if="usuario.doc_grado">
              <embed :src="usuario.doc_grado" height="400px" width="100%" v-if="endsWith(usuario.doc_grado,'pdf')" />
              <img :src="usuario.doc_grado" height="400px" width="100%"
                v-if="endsWith(usuario.doc_grado,'jpg') || endsWith(usuario.doc_grado,'png') || endsWith(usuario.doc_grado,'jpeg')" />
            </div>
            <div v-else>
              No se encontró ningún documento
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn oro btn-sm" data-dismiss="modal">Cerrar</button>

          </div>
        </div>
      </div>
    </div>

         
      <!-- Modal -->
      <div class="modal fade" id="confirmacionEnviar" tabindex="-1" role="dialog" aria-labelledby="confirm" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Confirmación de plantilla</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body centrado">
              ¿Estás seguro de enviar la plantilla?
            </div>
            <div class="modal-footer">
              <button type="button" @click="enviarPlantilla" class="btn verde" data-dismiss="modal">Enviar</button>
              <button type="button" class="btn oro" data-dismiss="modal">Cancelar</button>

            </div>
          </div>
        </div>
      </div>

    <!-- Modal -->
    <div class="modal fade" id="editarDatos" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title ">Editar datos personales</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="" id="editarDatosForm" method="post" enctype="multipart/form-data">
            <div class="modal-body row">
              <div class="form-group col-12 col-md-6">
                <label for="">Nombre completo</label>
                <input type="text" class="form-control-plaintext" name="" id="" aria-describedby="helpId"
                  :value="userAux.nombre+' '+userAux.materno+''" readonly>
              </div>
              <div class="form-group col-12 col-md-6">
                <label for="emailEdit">Email</label>
                <input type="email" class="form-control-plaintext" name="emailEdit" id="emailEdit"
                  aria-describedby="emailHelp" placeholder="Escribe tu correo" v-model="userAux.email" readonly>
              </div>

              <div class="form-group col-12 col-md-6">
                <label for="celularEdit">Celular</label>
                <input type="number" class="form-control" name="celularEdit" id="celularEdit"
                  aria-describedby="emailHelp" placeholder="Escribe tu número celular" v-model="userAux.celular">
                <small id="emailHelp" class="form-text text-muted">Requiere verificación de número</small>
              </div>



              <div class="form-group  col-12 col-md-6">
                <label for="tipoDocente">Tipo de docente</label>
                <select class="form-control" name="tipoDocente" id="tipoDocente" required v-model="usuario.id_tipo">
                  <option v-for="tipo in tiposUsuario" :value="tipo.id">{{tipo.nombre}}</option>
                </select>
              </div>

              <div class="form-group col-12 col-md-6">
                <label for="archivo">Archivo comprobante de último grado de estudios</label>
                <input type="file" class="form-control-file" name="archivo" id="archivo"
                  placeholder="Cargue el archivo comprobante" aria-describedby="fileHelpId">
                <small id="fileHelpId" class="form-text text-muted">*PDF,JPG o PNG</small>
              </div>
              <input type="text" class="collapse" value="subirArchivo" name="servicio" id="servicio">
              <input type="text" class="collapse" :value="id_usuario" name="id_usuario" id="id_usuario">





            </div>
            <div class="modal-footer">
              <button type="button" class="btn verde  btn-sm" onclick="subirArchivos()">Actualizar</button>
              <button type="button" class="btn oro  btn-sm" data-dismiss="modal">Cancelar</button>

            </div>
          </form>

        </div>
      </div>
      
 

    </div>

    <?php require 'partials/footer.php' ?>

  </div>
  <?php require 'partials/footerarchivos.php' ?>

  <script>
    function subirArchivos() {
      var parametros = new FormData($("#editarDatosForm")[0]);
      $.ajax({
        type: "POST",
        url: 'controladores/api.php',
        data: parametros,
        contentType: false,
        processData: false,
        success: function (response) {
          respuesta = JSON.parse(response);
          console.log(app.usuario.doc_grado);

          app.usuario = respuesta.usuario;
          console.log(app.usuario.doc_grado);
          $.notify(
            "Datos actualizados", {
              position: "right top",
              className: "success"
            }
          );

          if (respuesta.verificar) {
            alert('Se requiere verificar el número de celular');
            window.location = "validate.php"
          }
        }
      });
    }

    let app = new Vue({
      el: "#app",
      data: {
        lugarNull: {
          id: '',
          nombre: ''
        },
        tiposUsuario: [],
        usuario: JSON.parse('<?= json_encode($datos_profe)?>'),
        periodoActual: JSON.parse('<?= json_encode($periodo_actual)?>'),
        planes: JSON.parse('<?= json_encode($planes)?>'),
        horario: {

        },
        planSeleccionado: '',
        lugares: [],
        materiaSeleccionada: '',
        cargandoMaterias: false,
        materias: [],
        cargandoHorarios: false,
        horarios: [],
        id_usuario: '<?=$id?>',
        semana: [],
        tiempo_usuario: 0,
        materiasProfe: [],
        materiaProfeSeleccionada: '',
        horarioSeleccionado: {
          materia: '',
          semana: []
        },
        clase: {
          dia: -1,
          id_lugar: -1,
          entrada: '',
          salida: '',
          tiempo: 0,
          lugar: {
            id: '',
            nombre: ''
          }
        },
        horas: [
          '7:00',
          '7:30',
          '8:00',
          '8:30',
          '9:00',
          '9:30',
          '10:00',
          '10:30',
          '11:00',
          '11:30',
          '12:00',
          '12:30',
          '13:00',
          '13:30',
          '14:00',
          '14:30',
          '15:00',
          '15:30',
          '16:00',
          '16:30',
          '17:00',
          '17:30',
          '18:00',
          '18:30',
          '19:00',
          '19:30',
          '20:00',
          '20:30',
          '21:00',
          '21:30',
          '22:00',
        ],
        horaEntrada: -1,
        horaSalida: -1,
        horaEntrada2: -1,
        horaSalida2: -1,
        statusProfe: {
          clase: '',
          mensaje: '',
          inRango: false
        },
        pass: {
          actual: '',
          nueva: '',
          nueva2: '',
        },
        modelMateria: {
          id: ''
        },
        controlMateria: {
          total: 0,
          porcentaje: 0,
          semana: []
        },
        userAux: {
          url: ''
        },
        plantilla: {
          estado: null,
          id: null
        }
      },
      created: function () {
        this.getMateriasProfe();
        this.getHorarios();
        this.getLugares();
      },
      methods: {

        enviarPlantilla() {
          if(this.horarios.length > 0){
            $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "enviarPlantilla",
              id_profesor: this.id_usuario,
              horarios: this.horarios,
              id_plantilla: this.plantilla.id
            },
            success: function (response) {
              app.getHorarios();
              $.notify(
                  "Plantilla enviada para revisión, será notificado a su correo cuando se haya aprobado", {
                    position: "right top",
                    className: "success",

                  }
                );
            }
          });
          }else{
            $.notify(
                  "Agregue materias a su plantilla", {
                    position: "right top",
                    className: "warn",

                  }
                );
          }
         
        },
        endsWith(cadena, ext) {
          return cadena.endsWith(ext);
        },
        getTiposUsuario() {
          this.userAux = Object.assign({}, this.usuario);
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "getTipos"
            },
            success: function (response) {
              respuesta = JSON.parse(response);
              app.tiposUsuario = respuesta;
            }
          });
        },
        actualizarPass() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "putPass",
              id_usuario: this.id_usuario,
              actual: this.pass.actual,
              nueva: this.pass.nueva,
              correo: this.usuario.email
            },
            success: function (response) {


              if (response == 1) {
                $.notify(
                  "Contraseña actualizada y enviada a su correo", {
                    position: "right top",
                    className: "success",

                  }
                );
                $('#cambioPass').modal('hide');
                //location.replace("./logout.php");

              } else {
                $.notify(
                  "Ocurrió un error al actualizar la contraseña, verifique sus datos", {
                    position: "right top",
                    className: "danger"
                  });
              }

            }
          });
        },
        actualizarDatos() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "putUsuario",
              id_usuario: this.id_usuario,
              rfc: this.usuario.rfc,
              celular: this.usuario.celular,
              email: this.usuario.email
            },
            success: function (response) {
              respuesta = JSON.parse(response);
              if (respuesta) {
                app.usuario = respuesta;
                $.notify(
                  "Datos actualizados", {
                    position: "right top",
                    className: "success"
                  }
                );
              }
            }
          });
        },
        definirStatus() {
          if (this.usuario.minH && this.usuario.maxH) {
            if (this.tiempo_usuario < this.usuario.minH * 60) {
              this.statusProfe.clase = "alert rojo-1 mt-3 text-center";
              this.statusProfe.mensaje = " <b>Tiempo laboral fuera de rango </b><br> Restan " + this.formatMin(this
                .usuario
                .minH * 60 - this.tiempo_usuario) + " para llegar el mínimo de horas laborales ";
              this.statusProfe.inRango = false;
              return;
            }

            if (this.tiempo_usuario >= this.usuario.minH * 60 && this.tiempo_usuario < this.usuario.maxH * 60) {
              this.statusProfe.clase = "alert alert-verde-pastel mt-3 text-center";
              this.statusProfe.mensaje = "Dentro del rango <br> Tiempo adicional permitido: " + this.formatMin(this
                .usuario.maxH * 60 - this.tiempo_usuario);
              this.statusProfe.inRango = true;
              return;
            }

            if (this.tiempo_usuario == this.usuario.maxH * 60) {
              this.statusProfe.clase = "alert alert-warning mt-3 text-center";
              this.statusProfe.mensaje = "Ha llegado al maximo de horas clase permitidas";
              this.statusProfe.inRango = true;
              return;
            }
          }

          if (this.usuario.maxH) {
            if (this.tiempo_usuario < this.usuario.maxH * 60) {
              this.statusProfe.clase = "alert alert-verde-pastel text-center";
              this.statusProfe.mensaje = "Dentro del rango, tiempo de clase adicional permitido: " + this.formatMin(
                this
                .usuario.maxH * 60 - this.tiempo_usuario);
              this.statusProfe.inRango = true;
              return;
            }

            if (this.tiempo_usuario == this.usuario.maxH * 60) {
              this.statusProfe.clase = "alert alert-success text-center";
              this.statusProfe.mensaje = "Ha llegado al máximo de horas clase permitidas";
              this.statusProfe.inRango = true;
              return;
            }

            if (this.tiempo_usuario > this.usuario.maxH * 60) {
              this.statusProfe.clase = "alert rojo-1 text-center";
              this.statusProfe.mensaje = "Tiempo laboral fuera del rango, tiempo sobrante " + this.formatMin(this
                .tiempo_usuario - this.usuario.maxH * 60);
              this.statusProfe.inRango = false;
              return;
            }
          }
        },
        getTiempoUsuario() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "getTiempo",
              id_usuario: this.id_usuario
            },
            success: function (response) {
              var respuesta = JSON.parse(response);
              app.tiempo_usuario = respuesta.minutos;
              app.definirStatus();
            }
          });
        },
        getMaterias() {
          this.cargandoMaterias = true;
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "materias",
              id_plan: this.planSeleccionado
            },
            success: function (response) {
              respuesta = JSON.parse(response);
              app.materiaSeleccionada = "-1";
              app.materias = respuesta;
              app.cargandoMaterias = false;
            }
          });
        },
        getDia(numero) {
          if (numero == 1) return "Lunes";
          if (numero == 2) return "Martes";
          if (numero == 3) return "Miércoles";
          if (numero == 4) return "Jueves";
          if (numero == 5) return "Viernes";
          if (numero == 6) return "Sábado";
        },
        guardarMateria() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "guardarHorario",
              id_periodo: this.periodoActual.id,
              id_materia: this.modelMateria.id,
              id_profesor: this.id_usuario,
              semana: this.controlMateria.semana
            },
            success: function (response) {
              $.notify(
                "Materia agregada", {
                  position: "right top",
                  className: "success"
                }
              );
              $('#agregarMateria').modal('hide');
              app.getMateriasProfe();
              app.getHorarios();
            }
          });
        },
        getHorarios() {
          this.cargandoHorarios = true;
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "plantillaProfe",
              is_profe: true,
              id_profesor: this.id_usuario
            },
            success: function (response) {
              respuesta = JSON.parse(response);
              app.horarios = respuesta.horarios;
              console.log('hola')
              app.plantilla.id = respuesta.id;
              app.plantilla.estado = respuesta.estado;
              app.getTiempoUsuario();
              app.cargandoHorarios = false;

            }
          });
        },
        getMateriasProfe() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "getMateriasProfe",
              id_profesor: this.id_usuario
            },
            success: function (response) {
              respuesta = JSON.parse(response);
              app.materiasProfe = respuesta;
            }
          });
        },
        eliminarHorario(id_horario) {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "eliminarHorario",
              id_horario: id_horario
            },
            success: function (response) {
              $.notify(
                "Eliminación exitosa", {
                  position: "right top",
                  className: "info"
                }
              );
              app.getMateriasProfe();
              app.getHorarios();
            }
          });
        },
        nuevaMateria() {
          this.modelMateria = {
            id: ''
          };
          this.controlMateria.semana = [];
          this.controlMateria.porcentaje = 0;
          this.controlMateria.total = 0;
          this.clase = {
            dia: -1,
            lugar: Object.assign({}, this.lugarNull),
            entrada: '',
            salida: '',
            tiempo: 0,
            lugar: {
              id: '',
              nombre: ''
            }
          };
          this.entrada = -1;
          this.horaSalida = -1;
          this.planSeleccionado = '';
        },
        selecionarHorario(horario) {
          this.clase = {
            dia: -1,
            id_lugar: -1,
            entrada: '',
            salida: '',
            tiempo: 0,
            lugar: {
              id: '',
              nombre: ''
            }
          };
          this.horaEntrada = -1;
          this.horaSalida = -1;
          this.horarioSeleccionado = Object.assign({}, horario);

          this.horarioSeleccionado.porcentaje = (parseInt(this.horarioSeleccionado.minutos) * 100 / parseInt(this
            .horarioSeleccionado.total)).toFixed(2);
        },
        quitarClase2(index) {
          clase = this.controlMateria.semana[index];
          this.controlMateria.total -= clase.tiempo;
          this.controlMateria.porcentaje = ((this.controlMateria.total * 100) / parseInt(this.modelMateria.minutos))
            .toFixed(2);
          this.controlMateria.semana.splice(index, 1)
        },
        quitarClaseEdit(index) {

          clase = this.horarioSeleccionado.semana[index];
          this.horarioSeleccionado.total -= parseInt(clase.tiempo);

          this.horarioSeleccionado.porcentaje = ((this.horarioSeleccionado.total * 100) / parseInt(this
              .horarioSeleccionado.minutos))
            .toFixed(2);

          this.horarioSeleccionado.semana.splice(index, 1)
        },
        selecionarHoraEntrada(hora) {
          if (hora >= 0) {
            this.horaEntrada = this.horas[hora];
            this.horaSalida = -1;
          }
        },
        getLugares() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "getLugares"
            },
            success: function (response) {
              var respuesta = JSON.parse(response);
              app.lugares = respuesta;
            }
          });
        },
        toHoras(horaSalida, horaEntrada) {
          var diferencia = horaSalida - horaEntrada;

          horas = Math.floor((diferencia * 30) / 60);
          minutos = (diferencia * 30) % 60;
          mensaje = "";
          if (horas > 0) {
            if (horas == 1) mensaje = horas + " hora ";
            else mensaje = horas + " horas ";
          }

          if (minutos > 0) {
            if (mensaje) mensaje += "con " + minutos + " minutos";
            else mensaje = minutos + " minutos";
          }

          return mensaje;
        },
        editarHorario() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "putHorario",
              id_horario: this.horarioSeleccionado.id,
              semana: this.horarioSeleccionado.semana
            },
            success: function (response) {
              $.notify(
                "Cambios guardados", {
                  position: "right top",
                  className: "success"
                }
              );
              $('#editarHorario').modal('hide');
              app.getMateriasProfe();
              app.getHorarios();
            }
          });
        },
        guardarClase() {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "postclase",
              id_lugar: this.clase.id_lugar,
              dia: this.clase.dia,
              entrada: this.horas[this.horaEntrada],
              salida: this.horas[this.horaSalida],
              id_horario: this.horarioSeleccionado.id,
              tiempo: this.getMin(this.horas[this.horaSalida]) - this.getMin(this.horas[this.horaEntrada])
            },
            success: function (response) {
              $.notify(
                "Clase añadida exitosamente", {
                  position: "right top",
                  className: "success"
                }
              );
              app.getHorarios();
            }
          });
        },
        agregarClase() {

          if (this.clase.dia == -1) {
            $.notify(
              "Seleccione un lugar", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }

          if (!this.clase.lugar.id) {
            $.notify(
              "Seleccione un lugar", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }

          if (this.horaEntrada2 == -1) {
            $.notify(
              "Seleccione hora de entrada", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }

          if (this.horaSalida2 == -1) {
            $.notify(
              "Seleccione hora de salida", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }




          var clase = {
            l: this.clase.lugar.id,
            lugar: this.clase.lugar.nombre,
            d: this.clase.dia,
            e: this.horas[this.horaEntrada2],
            s: this.horas[this.horaSalida2],
            tiempo: this.getMin(this.horas[this.horaSalida2]) - this.getMin(this.horas[this.horaEntrada2])
          };
          this.clase.dia = -1;
          this.horaEntrada2 = -1;
          this.horaSalida2 = -1;
          this.clase.lugar = Object.assign({}, this.lugarNull);
          this.controlMateria.total += clase.tiempo;
          this.controlMateria.porcentaje = ((this.controlMateria.total * 100) / parseInt(this.modelMateria.minutos))
            .toFixed(2);


          this.controlMateria.semana.push(clase);
        },
        agregarClaseEdit() {

          if (this.clase.dia == -1) {
            $.notify(
              "Seleccione un lugar", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }

          if (!this.clase.lugar.id) {
            $.notify(
              "Seleccione un lugar", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }

          if (this.horaEntrada == -1) {
            $.notify(
              "Seleccione hora de entrada", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }

          if (this.horaSalida == -1) {
            $.notify(
              "Seleccione hora de salida", {
                position: "top center",
                className: "info"
              }
            );
            return;
          }




          var clase = {
            l: this.clase.lugar.id,
            lugar: this.clase.lugar.nombre,
            d: this.clase.dia,
            e: this.horas[this.horaEntrada],
            s: this.horas[this.horaSalida],
            tiempo: this.getMin(this.horas[this.horaSalida]) - this.getMin(this.horas[this.horaEntrada]),
            sigla: this.clase.lugar.sigla
          };
          this.clase.dia = -1;
          this.horaEntrada = -1;
          this.horaSalida = -1;
          this.clase.lugar = Object.assign({}, this.lugarNull);
          this.horarioSeleccionado.total += clase.tiempo;
          this.horarioSeleccionado.porcentaje = ((this.horarioSeleccionado.total * 100) / parseInt(this
              .horarioSeleccionado.minutos))
            .toFixed(2);

          this.horarioSeleccionado.semana.push(clase);

          if (parseInt(this.horarioSeleccionado.porcentaje) == 100) {
            $.notify(
              "Tiempo completado, ya puede guardar los cambios", {
                position: "top center",
                className: "info"
              }
            );
          }

        },
        quitarClase(id) {
          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "deleteclase",
              id_clase: id
            },
            success: function (response) {
              $.notify(
                "Eliminado", {
                  position: "right top",
                  className: "info"
                }
              );
              app.getHorarios();
            }
          });
        },
        getMin(hora) {
          data = hora.split(':');
          h = parseInt(data[0]);
          m = parseInt(data[1]);
          return (h * 60) + m;
        },
        formatMin(diferencia) {
          if (!diferencia) return "0 minutos";
          horas = Math.floor(diferencia / 60);
          minutos = diferencia % 60;
          mensaje = "";
          if (horas > 0) {
            if (horas == 1) mensaje = horas + " hora ";
            else mensaje = horas + " horas ";
          }

          if (minutos > 0) {
            if (mensaje) mensaje += "con " + minutos + " minutos";
            else mensaje = minutos + " minutos";
          }

          return mensaje;
        },
        calcularPorcentaje(total, valor) {
          total = parseInt(total);
          valor = parseInt(valor);
          porcenteje = (valor * 100 / total).toFixed(2);
          return porcenteje;
        },
        getClase(porcentaje) {
          clase = 'pull-right m-0 p-0 alert ';

          if (porcentaje <= 50) {
            clase += 'alert-danger';
          }

          if (porcentaje <= 90 && porcentaje > 50) {
            clase += 'alert-warning';
          }

          if (porcentaje < 100 && porcentaje > 90) {
            clase += 'alert-warning';
          }

          if (porcentaje == 100) {
            clase += 'collapse';
          }

          return clase;
        },
        toInt(valor) {
          return parseInt(valor);
        },
        quitarPuntitos(url) {
          if (url) {
            return url.replace("../", "");

          } else {
            return ''
          }
        },
        setStatusH(estado, id_horario) {

          $.ajax({
            type: "POST",
            url: 'controladores/api.php',
            data: {
              servicio: "setStatusH",
              id_horario: id_horario,
              estado: estado
            },
            success: function (response) {
              if (estado == 3) {
                $.notify(
                  "Horario enviado para su aprobación", {
                    position: "top center",
                    className: "success"
                  }
                );
              }

              if (estado == 0) {
                $.notify(
                  "Proponga un nuevo horario", {
                    position: "top center",
                    className: "info"
                  }
                );
              }

              app.getHorarios();
            }
          });
        }
      },
      computed: {
        formatDiferencia() {
          return this.formatMin(parseInt(this.modelMateria.minutos) - this.controlMateria.total);
        },
        verificadorHora() {
          if (this.controlMateria.porcentaje == 100) {
            $.notify(
              "Tiempo completado, ya puede guardar el horario", {
                position: "top center",
                className: "info"
              }
            );
          }
          return '';
        },



      }
    });
  </script>
</body>

</html>