

<?php

require '../Model.php';
session_start();

$modelo = new Model();

$servicio = $_POST['servicio'];

switch ($servicio) {
  case 'getTipos':
    $tipos = $modelo->getTiposProfe();
    echo json_encode($tipos);
    break;
  case 'getTiempo':
    $id_usuario = $_POST['id_usuario'];
    $tiempo = $modelo->getTiempoProfe($id_usuario);

    echo json_encode($tiempo);
    break;
  case 'horario':
    $datos = $modelo->getPeriodoActual();
    echo json_encode(array('success' => 1, 'data' => $datos));
    break;
  case 'materias':
    $id_plan = $_POST['id_plan'];
    $materias = $modelo->getMaterias($id_plan); 
    echo json_encode($materias); 
    break;
  case 'guardarHorario':
    $id_periodo = $_POST['id_periodo'];
    $id_materia = $_POST['id_materia'];
    $id_profesor = $_POST['id_profesor'];
    $semana = $_POST['semana'];
    $respuesta = $modelo->saveHorario($id_periodo,$id_materia,$id_profesor,$semana); 
    echo json_encode(array('success' => $respuesta));
  break;

  case 'getHorario':
    $id_profesor = $_POST['id_profesor'];
    $id_materia = $_POST['id_materia'];
    //Obtener el periodo actual
    $id_periodo = $modelo->getPeriodoActual()['id'];
    if(!is_null($id_periodo)){
      $horarios = $modelo->getHorarioProfe($id_profesor,$id_periodo,$id_materia);
    }
    else $horarios = array();
    echo json_encode($horarios); 
    break;
  case 'getMateriasProfe':
    $id_profesor = $_POST['id_profesor'];
    //Obtener el periodo actual
    $id_periodo = $modelo->getPeriodoActual()['id'];
    if(!is_null($id_periodo)){
      $horarios = $modelo->getMateriasProfe($id_profesor,$id_periodo);
    }
    else $horarios = array();
    echo json_encode($horarios); 
    break;
  case 'eliminarHorario':
    $id_horario = $_POST['id_horario'];
    $respuesta = $modelo->eliminarHorario($id_horario);
    echo json_decode(array('success' => $respuesta));
    break;
  case 'profesores':
    $profesores = $modelo->getProfesores();
    echo json_encode($profesores);
    break;
  case 'getperiodos':
    $periodos = $modelo->getPeriodos();
    echo json_encode($periodos);
    break;
  case 'postperiodo':
      $nombre = $_POST['nombre'];
      $respuesta = $modelo->savePeriodo($nombre);
      echo json_encode(array('success' => $respuesta));    
      break;
    case 'setperiodo':
      $id_periodo = $_POST['id_periodo'];
      $status = $_POST['status'];
      $respuesta = $modelo->setPeriodo($id_periodo,$status);
      echo json_encode(array('success' => $respuesta));    
      break;
  case 'deleteperiodo':
    $id_periodo = $_POST['id_periodo'];
    $respuesta = $modelo->deletePeriodo($id_periodo);
    echo json_encode($respuesta);    
    break;
  case 'putperiodo':
    $nombre = $_POST['nombre'];
    $id_periodo = $_POST['id_periodo'];
    $respuesta = $modelo->updatePeriodo($id_periodo,$nombre);
    echo json_encode(array('success' => $respuesta));  
    break;
  case 'postclase':
    $id_horario = $_POST['id_horario'];
    $id_lugar = $_POST['id_lugar'];
    $dia = $_POST['dia'];
    $entrada = $_POST['entrada'];
    $salida = $_POST['salida'];
    $tiempo = $_POST['tiempo'];
    $respuesta = $modelo->saveDia($id_horario,$dia,$id_lugar,$entrada,$salida,$tiempo);
    echo json_encode(array('success' => $respuesta));  
    
    break;
    case 'getLugares':
      $lugares = $modelo->getLugares();
      echo json_encode($lugares); 

      break;
    case 'deleteclase':
      $id_clase = $_POST['id_clase'];
      $respuesta = $modelo->deleteClase($id_clase);
      echo json_encode(array('success' => $respuesta));  
      break;

    case 'putUsuario':
      $id_usuario = $_POST['id_usuario'];
      $rfc = $_POST['rfc'];
      $celular = $_POST['celular'];
      $email = $_POST['email'];
      $respuesta = $modelo->updateUsuario($id_usuario,$rfc,$celular,$email);
      echo json_encode($respuesta);
      break;
    case 'putPass':
      $id_usuario = $_POST['id_usuario'];
      $nueva = $_POST['nueva'];
      $actual = $_POST['actual'];
      $correo = $_POST['correo'];
      $respuesta = $modelo->updatePass($id_usuario,$nueva,$actual,$correo);
      echo $respuesta;
        break;
    case 'recuperarPass':
    $correo = $_POST['correo'];
    $respuesta = $modelo->recuperarPass($correo);
    echo json_encode($respuesta);
      break;
    case 'reenviarCodigo':
      $id_usuario = $_POST['id_usuario'];
      $tipo = $_POST['tipo'];
      $respuesta = $modelo->reenviar($id_usuario,$tipo);
      echo $respuesta;
      break;
    case 'putHorario':
      $id_horario = $_POST['id_horario'];
      $semana = $_POST['semana'];
      $respuesta = $modelo->updateHorario($id_horario,$semana); 
      echo json_encode(array('success' => $respuesta));
      break;
    case 'subirArchivo':
      
        $celular = $_POST['celularEdit'];
        
        $id_usuario = $_POST['id_usuario'];
        $tipo_usuario = $_POST['tipoDocente'];
        $archivo = $_FILES['archivo'];
        $filtro = array('image/jpeg', 'image/png', 'application/pdf');
        $ext = pathinfo(basename($archivo["name"]), PATHINFO_EXTENSION);
        if (in_array($archivo["type"], $filtro)){
          $ruta = "archivos/".(new \DateTime())->format('Y-m-d-H-i-s').".".$ext;
          move_uploaded_file($archivo['tmp_name'],"../".$ruta);
          
          $respuesta = $modelo->updateUsuario2($id_usuario,$celular,$ruta,$tipo_usuario);
          echo json_encode($respuesta);
          
        }
        else{
          $respuesta = $modelo->updateUsuario2($id_usuario,$celular,null,$tipo_usuario);
          echo json_encode($respuesta);
        }

        if($respuesta['verificar']){
          $_SESSION['validar'] = $id_usuario;
          $_SESSION['user_id'] = null;
        }
      break;
    case 'getDatosProfe':
      $id_profe = $_POST['id_profe'];
      $datos = $modelo->getDatosProfe($id_profe);
      echo json_encode($datos);
      break;
    case 'setStatusH':
      $id_horario = $_POST['id_horario'];
      $estado = $_POST['estado'];
      $respuesta = $modelo->setStatusH($id_horario,$estado);
      echo $estado;
      break;

    case 'enviarPlantilla':
      $id_profesor = $_POST['id_profesor'];
      $horarios = $_POST['horarios'];
      $id_plantilla = $_POST['id_plantilla'];
      $id_periodo = $modelo->getPeriodoActual()['id'];
      $respuesta = $modelo->crearPlantilla($id_profesor,$id_periodo,$horarios,$id_plantilla);
      if(!$respuesta) return 0;
      return 1;
      break;
    case 'plantillaProfe':
      $id_profesor = $_POST['id_profesor'];
      $is_profesor = $_POST['is_profe'];
      $id_periodo = $modelo->getPeriodoActual()['id'];
      $plantilla = $modelo->getPlantillaProfe($id_profesor,$id_periodo,$is_profesor);
      echo json_encode($plantilla);
      break;
  default:
    echo json_encode(array('success' => 0, 'error' => 'Servicio no encontrado:'.$servicio));  
    break;
}