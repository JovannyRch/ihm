<?php
  session_start();

  require 'config/database.php';
  require 'Model.php';

  if (isset($_SESSION['user_id'])) {
    $id = $_SESSION['user_id'];
    
    $modelo = new Model();
    $periodo_actual = $modelo->getPeriodoActual();
    $datos_profe = $modelo->getDatosProfe($id);
    $planes = $modelo->getPlanes();
  }else{
    header("Location: /ihm/login.php");
}
?>