<?php

require('PHPMailer/PHPMailerAutoload.php');
  require('PHPMailer/src/PHPMailer.php');
  require('PHPMailer/src/SMTP.php');
  require('PHPMailer/src/Exception.php');
  require('fpdf/fpdf.php');
  class PDF extends FPDF
    {
       //Cabecera de página
       function Header()
       {
    
           $this->Image('D:/XAMPP/htdocs/ihm/logo.png',10,20,120,35);
    
    
       }
       function Footer()
       {
    
        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('Arial','I',8);
        // Print centered page number
        $this->Cell(0,10,'Sujeto a posibles modificaciones.',0,0,'C');
    
    
       }
    }
class Model{
    
    function __construct() {
        
        $server = 'localhost';
        $username = 'root';
        $password = '';
        $database = 'ihm';

        try {
            $this->db = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
        } catch (PDOException $e) {
            die('Connection Failed: ' . $e->getMessage());
        }

    }

   
    
    function reportePeriodo($id_periodo){
        $sql = "SELECT u.id,u.nombre,u.paterno,u.materno 
        FROM usuarios u where u.tipo_usuario = 1";
        $profesores =  $this->arreglo($sql);
        foreach ($profesores as &$p) {
            $horarios = $this->getHorarioProfe2($p['id'],$id_periodo);
            $p['horarios'] = $horarios;
        }
        return $profesores;
    }

    function crearPlantilla($id_profesor,$id_periodo,$horarios,$id_plantilla){
        
        if(!$id_plantilla){
            $respuesta = $this->query("INSERT INTO PLANTILLAS(id_profesor,id_periodo,estado) values($id_profesor,$id_periodo,0)");
            $id_platilla = $this->db->lastInsertId();
        }
        if($respuesta){
           
           
            foreach ($horarios as $horario ) {
                $sql = "UPDATE horarios set id_plantilla = $id_platilla, `status` = 3 where id = ".$horario['id'];
                $respuesta = $this->query($sql);
                if(!$respuesta) return false;
            }

            return true;
        }
        return null;
    }

    function getPlantillaProfe($id_profesor,$id_periodo,$isProfe = false){
        $plantilla = $this->registro("SELECT id,estado from plantillas where id_profesor = $id_profesor and id_periodo = $id_periodo");
        $id_platilla = $plantilla['id'];
        
        if(!$id_platilla){
            if($isProfe){
                $id_periodo_actual = $this->getPeriodoActual()['id'];
                $horarios = $this->getHorarioProfe($id_profesor,$id_periodo_actual);
                return array('id' => null, 'status' => null, 'horarios' => $horarios);
            }   
            else {
                 return null; 
            }
        }
        $sql = "SELECT h.*,m.nombre materia,
        m.minutos,
        (SELECT nombre from planes where id = m.id_plan) plan, 0 as total,0 as porcentaje
        from horarios h inner join materias m on m.id = h.id_materia  
        where id_plantilla = $id_platilla order by h.id desc";
        $horarios =  $this->arreglo($sql);
        


        foreach ($horarios as &$horario) {
            $horario['semana'] = $this->getSemana($horario['id']);
            $id_horario = $horario['id'];
            $horario['total'] = $this->registro("SELECT sum(tiempo) total from clases where id_horario = $id_horario")['total'];
            
        }
        $plantilla['horarios'] = $horarios;
        return $plantilla;
    }
    
    function updateDatos($archivo){

    }

    function getTiposProfe(){
        return $this->arreglo("SELECT * from tipo_profesores");
    }

    function registro($sql){
        $resultado = $this->db->prepare($sql);
        if($resultado->execute() ){
            $arreglo =  $this->utf8_converter($resultado->fetchAll(PDO::FETCH_ASSOC));
            if(sizeof($arreglo) > 0){
                return $arreglo[0];
            }
            else return null;
        }else return null;
    }

    function arreglo($sql){
        $resultados = $this->db->prepare($sql);
        if($resultados->execute()){
            return $this->utf8_converter($resultados->fetchAll(PDO::FETCH_ASSOC));
        }else return null;
    }

    function getHorario($id){
        return $this->arreglo("SELECT * from horarios where id_profesor = $id");  
    }


    function getPeriodoActual(){
        return $this->registro("SELECT id,nombre from periodos where actual = 1");          
    }

    function getDatosProfe($id){
        $sql = "SELECT u.id, u.email, u.rfc,u.oficina,u.nombre,u.paterno,u.materno,u.celular,u.doc_grado,p.nombre tipo_profesor,p.id id_tipo,u.rfc,p.minH,p.maxH
        FROM usuarios u inner join tipo_profesores p on u.id_tipo_profesor = p.id WHERE u.id = $id";
        return $this->registro($sql);  

    }

    function getTiempoProfe($id){
        $id_periodo_actual = $this->getPeriodoActual()['id'];
        $sql = "SELECT sum(tiempo) minutos from clases c where c.id_horario in (SELECT h.id from horarios h where h.id_profesor = $id and h.id_periodo = $id_periodo_actual)";
        return $this->registro($sql);
    }


    function getDatosAdministrador($id){
        $sql = "SELECT u.id, u.email, u.nombre,u.paterno,u.materno,u.celular FROM usuarios u WHERE u.id = $id";
        return $this->registro($sql);  
    }

    function getPlanes(){
        return $this->arreglo("SELECT * from planes"); 
    }


    function getMaterias($id_plan){
        return $this->arreglo("SELECT id,nombre,minutos from materias where id_plan = $id_plan order by nombre asc");
    }

    
    function saveHorario($id_periodo,$id_materia,$id_profesor,$semana){
        $sql = "INSERT into horarios(id_periodo,id_materia,id_profesor,`status`) values($id_periodo,$id_materia,$id_profesor,0)";
        $stmt = $this->db->prepare($sql);
        if($stmt->execute()){
            $id_horario = $this->db->lastInsertId();
            foreach ($semana as $dia) {
               if(!$this->saveDia($id_horario,$dia['d'],$dia['l'],$dia['e'],$dia['s'],$dia['tiempo'])) return false;
            }
          
            return true;

        }
        return null;
    }

    function updateHorario($id_horario,$semana){
        if($this->query("DELETE FROM clases where id_horario = $id_horario")){
            foreach ($semana as $dia) {
                if(!$this->saveDia($id_horario,$dia['d'],$dia['l'],$dia['e'],$dia['s'],$dia['tiempo'])) return false;
             }
             return true;
        }
        return null;
    }

    


    //Horarios de un profesor en un periodo 
    function getHorarioProfe($id_profesor,$id_periodo,$id_materia = null){

        if(!$id_materia){
            $horarios =  $this->arreglo("SELECT h.*, m.nombre materia,
             m.minutos,
             (SELECT nombre from planes where id = m.id_plan) plan,  0 as total,0 as porcentaje
            from horarios h inner join materias m on m.id = h.id_materia  
            where h.id_profesor = $id_profesor and h.id_periodo = $id_periodo order by h.id desc");
        }
        else{
            $horarios =  $this->arreglo("SELECT h.*,
            m.nombre materia,
            m.minutos,
            (SELECT nombre from planes where id = m.id_plan) plan, 0 as total,0 as porcentaje
            from horarios h inner join materias m on m.id = h.id_materia  
            where h.id_profesor = $id_profesor and h.id_periodo = $id_periodo and m.id = $id_materia order by h.id desc");
        }
        $horariosChido = array();
        foreach ($horarios as $horario) {
            $horario['semana'] = $this->getSemana($horario['id']);
            $id_horario = $horario['id'];
            $horario['total'] = $this->registro("SELECT sum(tiempo) total from clases where id_horario = $id_horario")['total'];
            $horariosChido[] = $horario;
        }
        return $horariosChido;
        
    }

    function getHorarioProfe2($id_profesor,$id_periodo){

        $horarios =  $this->arreglo("SELECT h.*, m.nombre materia,
        m.minutos,
        (SELECT nombre from planes where id = m.id_plan) plan,  0 as total,0 as porcentaje
       from horarios h inner join materias m on m.id = h.id_materia  
       where h.id_profesor = $id_profesor and h.id_periodo = $id_periodo order by h.id desc");
        $horariosChido = array();
        
        foreach ($horarios as $horario) {
            $horario['semana'] = $this->getSemana2($horario['id']);
            $id_horario = $horario['id'];
            $horariosChido[] = $horario;
        }
        return $horariosChido;
        
    }

    function getSemana2($id_horario){
        $semana = array();
        for ($dia=1; $dia <= 6; $dia++) { 
            $sql = "SELECT dia d,id,he e,hs s, (select nombre from lugares where lugares.id = clases.id_lugar) l from clases where id_horario = $id_horario and dia = $dia";
            $data = $this->registro($sql);
            if(is_null($data['id'])){
                $data = 
                array(
                    'd' => $dia,
                    'e' => '',
                    's' => '',
                    'l' => ''
                );
            }
            $semana[] = $data;
        }

        return $semana;
        }

    function getSemana($id_horario){
        /*$semana = array();
        for ($dia=1; $dia <= 6; $dia++) { 
            $sql = "SELECT dia d,id,he e,hs s, (select nombre from lugares where lugares.id = dias.id_lugar) l from dias where id_horario = $id_horario and dia = $dia";
            $data = $this->registro("SELECT dia d,id,he e,hs s, (select nombre from lugares where lugares.id = dias.id_lugar) l from dias where id_horario = $id_horario and dia = $dia");
            if(is_null($data['id'])){
                $data = 
                array(
                    'd' => $dia,
                    'e' => '',
                    's' => '',
                    'l' => ''
                );
            }
            $semana[] = $data;
        }

        return $semana;*/
        return $this->arreglo("SELECT dia d,he e,hs s, clases.tiempo,lugares.id l,lugares.nombre lugar,lugares.sigla from clases inner join lugares on lugares.id = clases.id_lugar where id_horario = $id_horario order by dia asc");
    }


    function saveDia($id_horario,$dia,$id_lugar,$entrada,$salida,$tiempo){
        return $this->query("INSERT into clases(id_horario,dia,id_lugar,he,hs,tiempo) 
        values($id_horario,$dia,$id_lugar,'$entrada','$salida',$tiempo)");
    }

    function getMateriasProfe($id_profesor,$id_periodo){
        $materias =  $this->arreglo("SELECT DISTINCT m.id,m.nombre,(SELECT nombre from planes where id = m.id_plan) plan
        from horarios h inner join materias m on m.id = h.id_materia
        where h.id_profesor = $id_profesor and h.id_periodo = $id_periodo order by h.id desc");

        return $materias;
    }


    function utf8_converter($array)
    {
        array_walk_recursive($array, function(&$item, $key){
            if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
            } 
        });

        return $array;
    }

    function eliminarHorario($id_horario){
        return $this->query("DELETE from horarios where id = $id_horario");
    }

    function query($sql){
        $stmt = $this->db->prepare($sql);
        return $stmt->execute();
    }


    function getProfesores(){
        $periodoActual = $this->getPeriodoActual();
        if(!$periodoActual['id']) return array();
        $sql = "SELECT u.id, u.email, u.oficina,u.nombre,u.paterno,u.materno,u.celular,u.doc_grado,p.nombre tipo_profesor,p.id id_tipo,u.rfc,p.minH,p.maxH
        FROM usuarios u inner join tipo_profesores p on u.id_tipo_profesor = p.id";
        $profesores =  $this->arreglo($sql);
        $resultado = array();
        foreach ($profesores as &$p) {
            if($this->getPeriodoActual()['nombre']){
                $p['tiempo'] = $this->getTiempoProfe($p['id']); 
            }else{
                $p['tiempo'] = 0;
            }
        }
        return $profesores;
    }

    function getPeriodos(){
        return array('periodos' => $this->arreglo("SELECT * from periodos order by actual desc,id asc"), 'actual' => $this->getPeriodoActual());
    }

    function savePeriodo($nombre){
        return $this->query("INSERT into periodos(nombre) values('$nombre')");
    }


    function setPeriodo($id_periodo,$status){
        if($status == 1){
            $this->query("UPDATE periodos set actual = 0");
        }
        return $this->query("UPDATE periodos set actual = $status where id = $id_periodo");
    }

    function deletePeriodo($id_periodo){
        $mensaje = "";
        $success = 0;
        if($id_periodo){
            $sql = "DELETE from periodos where id = $id_periodo";
            $ok = $this->query($sql);
            if($ok){
                $mensaje = "Eliminado con exito";
                $success = 1;
            }
            else $mensaje = "Existen registros guardados en el periodo";
            return array(
                'success' => $ok,
                'mensaje' => $mensaje
            );
        }
        return array(
            'success' => 0,
            'mensaje' => 'ID no encontrado'
        );
    }
    


    function getLugares(){
        return $this->arreglo("SELECT * from lugares");
    }

    function updatePeriodo($id_periodo,$nombre){
        return $this->query("UPDATE periodos set nombre = '$nombre' where id = $id_periodo");
    }

    function deleteClase($id_clase){
        return $this->query("DELETE from clases where id = $id_clase");
    }


    function updateUsuario($id_usuario,$rfc,$celular,$email){
        $sql = "UPDATE usuarios set rfc = '$rfc', celular = $celular, email = '$email' where id = $id_usuario";
        if($this->query($sql)){
            return $this->getDatosProfe($id_usuario);
        }return null;
    }

    function updateUsuario2($id_usuario,$celular,$ruta = null,$tipo_usuario){
        $numeroRegistrado = $this->registro("SELECT celular from usuarios where id = $id_usuario")['celular'];
        $correo = $this->registro("SELECT email from usuarios where id = $id_usuario")['email'];
        $verificacionSMS = $this->registro("SELECT codigo_verificacion from validacion INNER JOIN usuarios on validacion.id_profesor = usuarios.id WHERE email = '$correo' AND tipo_verificacion = 1")['codigo_verificacion'];
        $respuesta = array('verificar' => false, 'usuario' => array());
        if($celular != $numeroRegistrado){
            $respuesta['verificar'] = true; //Dejar esta linea
            //Volver a verificar, cambiar el estado de verificacion en la tabla validacion
            //Comenzar aqui
            $sql = "SELECT verificado from validacion INNER JOIN usuarios on validacion.id_profesor = usuarios.id WHERE email = '$correo' AND tipo_verificacion = 1";
            $stmt1 = $this->db->prepare($sql);
            if($stmt1->execute()){
                $num1 = $stmt1->rowCount();
                //$verificado1=  $this->registro("SELECT verificado from validacion INNER JOIN usuarios on validacion.id_profesor = usuarios.id WHERE email = '$correo' AND tipo_verificacion = 1")['verificado'];
                $verificado1= $stmt1->fetch(PDO::FETCH_ASSOC)['verificado'];

                if ($verificado1 == 1) {
                                         
                       $query1 = "UPDATE validacion
                       set verificado = '0'
                       where codigo_verificacion = '$verificacionSMS'";
                     
                        //$stmt2 = $this->query($query1);
                        $stmt = $this->db->prepare($query1);

                        
                     if( $num1>0){ //Verifica que existan esos codigos en la bd
                        if($stmt->execute()){
                        // tell the user
                       // $messageSuccess = "Tu correo ha sido VALIDADO CORRECTAMENTE!. Ahora puedes iniciar sesión";
                       //$verificationCodeSMS = crc32(uniqid(rand(), true));
                       $username = "jorgeramirezmanzanares@gmail.com"; 
                       $hash = "3945f93e9409a476a699e06170284206fd42b643579e01df26a2b7b40a67b638"; 
                       $test = "0"; 
                       $sender = "API Test"; // This is who the message appears to be from. 
                       $numbers = "52".$celular; // A single number or a comma-seperated list of numbers 
                       $message = "Tu código de verificación es: ".$verificacionSMS; 
                 
                       $message = urlencode($message); 
                       $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test; 
                       $ch = curl_init('http://api.txtlocal.com/send/?'); 
                       curl_setopt($ch, CURLOPT_POST, true); 
                       curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
                       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                       $result = curl_exec($ch); // This is the result from the API 
                      
                       curl_close($ch); 
                      // echo($result); 
                        
                        $_SESSION['msg'] = array("tipo" => "verde-pastel", "texto" => "Tú teléfono ha sido cambiado EXITOSAMENTE! Te hemos enviado un SMS con el nuevo codigo de verificacion");


                        }else{
                       
                        }
                      }else{
                        $_SESSION['msg'] = array("tipo" => "verde-pastel", "texto" => "ERROR");
                      }
                     
                    } 
            }
        }
        $subquery = '';
        if($ruta){
            $subquery = "doc_grado = '$ruta',";
        }
        $sql = "UPDATE usuarios set celular = $celular,$subquery id_tipo_profesor = $tipo_usuario where id = $id_usuario";
        if($this->query($sql)){
            $respuesta['usuario'] = $this->getDatosProfe($id_usuario);
            return $respuesta;
        }return array('verificar' => false, 'usuario' => array());
    }

    function updatePass($id_usuario,$nueva,$actual,$correo){
        $passActual = $this->registro("SELECT `pass` from usuarios where id = $id_usuario")['pass'];
        if(strcasecmp($actual, $passActual) == 0){
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = '587';
                             //465
            $mail->isHTML(true);
            $mail->CharSet = 'UTF-8';
            $mail->Username = 'uaemex.ico.validar@gmail.com';
            $mail->Password = 'Universidad19';
            $mail->SetFrom('uaemex.ico.validar@gmail.com','no-reply@ico.uaemex.mx');
            $mail->Subject = "CAMBIO DE CONTRASEÑA";
            $mail->AddAddress($correo);
           // $sql = "SELECT id from usuarios where email = '$correo'";
            $htmlStr = "";
           // $verificationCode = md5(uniqid("yourrandomstringyouwanttoaddhere", true));
            $htmlStr .= "Tu usuario es: " . $correo . " <br /> Y tu nueva contraseña es: " .$nueva;
            $mail->Body = $htmlStr;
            if( !$mail->send()){
               // echo "Mailer Error: " . $mail->ErrorInfo;

                die("Sending failed.");
            }else{
              //  echo 'Se ha enviado la nueva contraseña al correo' . $correo ;
                return $this->query("UPDATE usuarios set `pass` = '$nueva' where id = $id_usuario");


            }

        }else {
            return false;
        }
    }

    function recuperarPass($correo){
        $sql = "SELECT email from usuarios where email = '$correo'";
      
        $verificacion = $this->registro($sql)['email'];
        
        if($verificacion){
            $passNew = $this->generateRandomString(6);
            $sql2 = "UPDATE usuarios set `pass` = '$passNew' where email = '$correo'";
            
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = '587';
                             //465
            $mail->isHTML(true);
            $mail->CharSet = 'UTF-8';
            $mail->Username = 'uaemex.ico.validar@gmail.com';
            $mail->Password = 'Universidad19';
            $mail->SetFrom('uaemex.ico.validar@gmail.com','no-reply@ico.uaemex.mx');
            $mail->Subject = "RECUPERACIÓN DE CONTRASEÑA";
            $mail->AddAddress($correo);
           // $sql = "SELECT id from usuarios where email = '$correo'";
            $htmlStr = "";
           // $verificationCode = md5(uniqid("yourrandomstringyouwanttoaddhere", true));
            $htmlStr .= "Tu nueva contraseña es: " .$passNew;
            $mail->Body = $htmlStr;
            if( !$mail->send()){
               // echo "Mailer Error: " . $mail->ErrorInfo;

                die("Sending failed.");
            }else{
              //  echo 'Se ha enviado la nueva contraseña al correo' . $correo ;
              return $this->query($sql2);


            }
        }else return null;
    }

    function reenviar($id_usuario,$tipo){
        $correo = $this->registro("SELECT email from usuarios where id = $id_usuario")["email"];
        $codigo_verificacion = $this->registro("SELECT codigo_verificacion from validacion where id_profesor = $id_usuario and tipo_verificacion = $tipo")["codigo_verificacion"];
        $telefono = $this->registro("SELECT celular from usuarios where id = $id_usuario")["celular"];
    if($tipo==0){
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = '587';
                             //465
            $mail->isHTML(true);
            $mail->CharSet = 'UTF-8';
            $mail->Username = 'uaemex.ico.validar@gmail.com';
            $mail->Password = 'Universidad19';
            $mail->SetFrom('uaemex.ico.validar@gmail.com','no-reply@ico.uaemex.mx');
            $mail->Subject = "CODIGO DE VERIFICACION";
            $mail->AddAddress($correo);
           // $sql = "SELECT id from usuarios where email = '$correo'";
            $htmlStr = "";
           // $verificationCode = md5(uniqid("yourrandomstringyouwanttoaddhere", true));
            $htmlStr .= "Tu codigo de verificación  es: " .$codigo_verificacion;
            $mail->Body = $htmlStr;
            if( !$mail->send()){
               // echo "Mailer Error: " . $mail->ErrorInfo;

                die("Sending failed.");
            }else{
              //echo 'Se ha enviado el codigo de verificacion a: ' . $correo ;
              $_SESSION['msg'] = array("tipo" => "verde-pastel", "texto" => "EMAIL enviado con EXITO");


            }
        }

        if($tipo==1){
            $username = "jorgeramirezmanzanares@gmail.com"; 
            $hash = "3945f93e9409a476a699e06170284206fd42b643579e01df26a2b7b40a67b638"; 
            $test = "0"; 
            $sender = "API Test"; // This is who the message appears to be from. 
            $numbers = "52".$telefono; // A single number or a comma-seperated list of numbers 
            $message = "Tu código de verificación es: ".$codigo_verificacion; 
      
            $message = urlencode($message); 
            $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test; 
            $ch = curl_init('http://api.txtlocal.com/send/?'); 
            curl_setopt($ch, CURLOPT_POST, true); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            $result = curl_exec($ch); // This is the result from the API 
            curl_close($ch); 
            //echo($result); 
            return 1;
        }

        return 0;
    }


    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    function enviarSMS($numero,$mensaje){
        //Crear una funcion que envie mensaje
    }
    
 
    function enviarEmail($email,$asunto,$mensaje,$datosClase,$nombreMateria){
        //$dire=__DIR__."test.pdf";
        $dire="D:/XAMPP/htdocs/ihm/test.pdf";
        $mensaje = 'Estimado docente, a través de la presente la Faculta de Ingeniería le informa que su horario propuesto en el sistema de solicitud para ingreso a plantilla de horarios ha sido ACEPTADO.';
       
        
        $pdf=new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->Header();
        $pdf->SetFont('Arial','B',14);
        $pdf->Ln(60);
        $pdf->MultiCell(200,10,utf8_decode($mensaje));
        $pdf->Ln(1);
        $pdf->Cell(200,50,utf8_decode($nombreMateria),0,0,'C');
        $pdf->Ln(40);

        $lista = "";
        foreach ($datosClase as $dato) {
            $dia = $dato['dia'];
            $he = $dato['he'];
            $hs = $dato['hs'];
            
            switch($dia){
        
                case 1:
                $dia = 'LUNES';
                break;
                case 2:
                $dia = 'MARTES';
                break;
                case 3:
                $dia = 'MIÉRCOLES';
                break;
                case 4:
                $dia = 'JUEVES';
                break;
                case 5:
                $dia = 'VIERNES';
                break;
                case 6:
                $dia = 'SÁBADO';
                break;
                
            }

            $pdf->Cell(40,40,utf8_decode($dia).'    '.$he.'  -  '.$hs);
            $pdf->Ln(20);


        }
      
   
    
       
        

        $pdf->Output($dire,'F');

        $mail = new PHPMailer\PHPMailer\PHPMailer();
      $mail->isSMTP();
      $mail->SMTPAuth = true;
      $mail->SMTPSecure = 'tls';
      $mail->Host = 'smtp.gmail.com';
      $mail->Port = '587';
                       //465
      $mail->isHTML(true);
      $mail->CharSet = 'UTF-8';
      $mail->Username = 'uaemex.ico.validar@gmail.com';
      $mail->Password = 'Universidad19';
      $mail->SetFrom('uaemex.ico.validar@gmail.com','no-reply@ico.uaemex.mx');
      $mail->Subject = $asunto;
      $mail->AddAddress($email);
      
      $mail->Body = $mensaje;
      $mail->AddAttachment('D:/XAMPP/htdocs/ihm/test.pdf');

      if( !$mail->send()){
         echo "Mailer Error: " . $mail->ErrorInfo;

        // die("Sending failed.");
     }else{
       echo 'Se ha enviado el codigo de verificacion a: ' . $email ;
       //$_SESSION['msg'] = array("tipo" => "verde-pastel", "texto" => "EMAIL enviado con EXITO");


     }

    }

    function setStatusH($id_horario,$estado){
        if($estado == 1){
            $datosHorario = $this->registro("SELECT * from horarios where id = $id_horario");
            $datosMateria = $this->registro("SELECT id_plan,nombre from materias where id = ".$datosHorario['id_materia']);
            $datosProfesor = $this->registro("SELECT email,nombre,paterno,materno,celular from usuarios where id = ".$datosHorario['id_profesor']);
            $datosClases = $this->arreglo("SELECT dia,he,hs,id_lugar from clases where id_horario = $id_horario");
            $nombrePeriodo = $this->registro("SELECT nombre from periodos where id = ".$datosHorario['id_periodo'])['nombre'];
            $nombreMateria = $datosMateria['nombre'];
            $nombrePlan = $this->registro("SELECT nombre from planes where id = ".$datosMateria['id_plan'])['nombre'];
            $nombreProfesor = $datosProfesor['nombre'].' '.$datosProfesor['paterno'].' '.$datosProfesor['materno'];
           /* $horario = 'MATERIA: '.$nombreMateria.'     DÍA: '.$datosClases['dia'];
            $horario1 = 'HORA ENTRADA: '.$datosClases['he'].'HORA SALIDA: '.$datosClases['hs'];
            $horario2 = 'LUGAR: '.$datosClases['id_lugar'];*/
           


            $email = $datosProfesor['email'];
            $celular = $datosProfesor['celular'];
            $asunto = "Aprobación de horario periodo ".$nombrePeriodo;
            $mensaje = "Estimad@ $nombreProfesor, el horario de la materia $nombreMateria ($nombrePlan) ha sido APROBADO.";
           
            $this->enviarEmail($email,$asunto,$mensaje,$datosClases,$nombreMateria);



        }
        return $this->query("UPDATE horarios set `status` = $estado where id = $id_horario");
    }
}
